<?php

namespace App\Entity;

use App\Repository\ExtraCashRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ExtraCashRepository::class)
 */
class ExtraCash
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $safe; //Coffre

    /**
     * @ORM\Column(type="integer")
     */
    private $balanceUV;

    /**
     * @ORM\Column(type="integer")
     */
    private $currency; //Devise

    /**
     * @ORM\Column(type="integer")
     */
    private $debt; //Dette
    
    /**
     * @ORM\Column(type="integer")
     */
    private $canal;

    /**
     * @ORM\Column(type="integer")
     */
    private $other;
    
     /**
     * @ORM\Column(type="integer")
     */
    private $misc;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSafe(): ?int
    {
        return $this->safe;
    }

    public function setSafe(int $safe): self
    {
        $this->safe = $safe;

        return $this;
    }

    public function getBalanceUV(): ?int
    {
        return $this->balanceUV;
    }

    public function setBalanceUV(int $balanceUV): self
    {
        $this->balanceUV = $balanceUV;

        return $this;
    }

    public function getCurrency(): ?int
    {
        return $this->currency;
    }

    public function setCurrency(int $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getDebt(): ?int
    {
        return $this->debt;
    }

    public function setDebt(int $debt): self
    {
        $this->debt = $debt;

        return $this;
    }

    public function getOther(): ?int
    {
        return $this->other;
    }

    public function setOther(int $other): self
    {
        $this->other = $other;

        return $this;
    }
    
    public function getCanal() {
        return $this->canal;
    }

    public function getMisc() {
        return $this->misc;
    }

    public function setCanal($canal): void {
        $this->canal = $canal;
    }

    public function setMisc($misc): void {
        $this->misc = $misc;
    }


}
