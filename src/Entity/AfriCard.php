<?php

namespace App\Entity;

use App\Repository\AfriCardRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AfriCardRepository::class)
 */
class AfriCard
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $typeCard;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $buyPrice;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;
  
    /**
     * @ORM\Column(type="integer")
     */
    private $benefit; //COMMISSION SUR RECHARGEMENT
    
    /**
     * @ORM\Column(type="integer")
     */
    private $benefitPerCent; //COMMISSION SUR RECHARGEMENT SUPERIEUR A 75000F
  
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CardAgency", mappedBy="card")
     */
    private $agencyCards;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $sold;
    
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateSave;
    
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeCard(): ?string
    {
        return $this->typeCard;
    }

    public function setTypeCard(string $typeCard): self
    {
        $this->typeCard = $typeCard;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getBuyPrice() {
        return $this->buyPrice;
    }

    public function setBuyPrice($buyPrice): void {
        $this->buyPrice = $buyPrice;
    }

    public function getBenefit() {
        return $this->benefit;
    }

    public function getBenefitPerCent() {
        return $this->benefitPerCent;
    }

    public function setBenefit($benefit): void {
        $this->benefit = $benefit;
    }

    public function setBenefitPerCent($benefitPerCent): void {
        $this->benefitPerCent = $benefitPerCent;
    }

    public function getAgencyCards() {
        return $this->agencyCards;
    }

    public function setAgencyCards($agencyCards): void {
        $this->agencyCards = $agencyCards;
    }

    public function getQuantity() {
        return $this->quantity;
    }

    public function setQuantity($quantity): void {
        $this->quantity = $quantity;
    }

    public function getSold() {
        return $this->sold;
    }

    public function setSold($sold): void {
        $this->sold = $sold;
    }

    public function getDateSave() {
        return $this->dateSave;
    }

    public function setDateSave($dateSave): void {
        $this->dateSave = $dateSave;
    }

}
