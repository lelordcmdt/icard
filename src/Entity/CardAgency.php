<?php

namespace App\Entity;

use App\Repository\CardAgencyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CardAgencyRepository::class)
 */
class CardAgency
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="date")
     */
    private $dateAdd;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AfriCard", inversedBy="agencyCards")
     */
    private $card;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="agencyCards")
     */
    private $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Agency", inversedBy="agencyCards")
     */
    private $agency;
    
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getDateAdd(): ?\DateTimeInterface
    {
        return $this->dateAdd;
    }

    public function setDateAdd(\DateTimeInterface $dateAdd): self
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }
    
    public function getCard() {
        return $this->card;
    }

    public function getAgency() {
        return $this->agency;
    }

    public function setCard($card): void {
        $this->card = $card;
    }

    public function setAgency($agency): void {
        $this->agency = $agency;
    }
    
    public function getUser() {
        return $this->user;
    }

    public function setUser($user): void {
        $this->user = $user;
    }



}
