<?php

namespace App\Entity;

use App\Repository\OccasionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OccasionRepository::class)
 */
class Occasion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Operation", mappedBy="occasion")
     */
    private $operations;
    
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Client", mappedBy="occasion")
     */
    private $clients;
    
    /**
     * @ORM\Column(type="string", length=15)
     */
    private $typeOperation;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Heading", inversedBy="occasions")
     */
    private $heading;
    
    public function getClients() {
        return $this->clients;
    }

    public function setClients($clients): void {
        $this->clients = $clients;
    }

        public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
    
    

    public function getTypeOperation(): ?string
    {
        return $this->typeOperation;
    }

    public function setTypeOperation(string $typeOperation): self
    {
        $this->typeOperation = $typeOperation;

        return $this;
    }
    
    public function getOperations() {
        return $this->operations;
    }

    public function setOperations($operations): void {
        $this->operations = $operations;
    }

    public function getHeading() {
        return $this->heading;
    }

    public function setHeading($heading): void {
        $this->heading = $heading;
    }


}
