<?php

namespace App\Entity;

use App\Repository\BenefitRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BenefitRepository::class)
 */
class Benefit
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $orange;
    
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $moov;

    
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $mtn;
    
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $canalPlus;
    
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $wizall;
    
   
    /**
     * @ORM\Column(type="date")
     */
    private $dateBen;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="benefits")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $africard;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $moneygram;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $wu;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ria;
   

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateBen(): ?\DateTimeInterface
    {
        return $this->dateBen;
    }

    public function setDateBen(\DateTimeInterface $dateBen): self
    {
        $this->dateBen = $dateBen;

        return $this;
    }
    
    public function getUser() {
        return $this->user;
    }

    public function setUser($user): void {
        $this->user = $user;
    }

    public function getOrange() {
        return $this->orange;
    }

    public function getMoov() {
        return $this->moov;
    }

    public function getMtn() {
        return $this->mtn;
    }

    public function getCanalPlus() {
        return $this->canalPlus;
    }

    public function getWizall() {
        return $this->wizall;
    }

    public function setOrange($orange): void {
        $this->orange = $orange;
    }

    public function setMoov($moov): void {
        $this->moov = $moov;
    }

    public function setMtn($mtn): void {
        $this->mtn = $mtn;
    }

    public function setCanalPlus($canalPlus): void {
        $this->canalPlus = $canalPlus;
    }

    public function setWizall($wizall): void {
        $this->wizall = $wizall;
    }

    public function getAfricard(): ?string
    {
        return $this->africard;
    }

    public function setAfricard(?string $africard): self
    {
        $this->africard = $africard;

        return $this;
    }

    public function getMoneygram(): ?string
    {
        return $this->moneygram;
    }

    public function setMoneygram(?string $moneygram): self
    {
        $this->moneygram = $moneygram;

        return $this;
    }

    public function getWu(): ?string
    {
        return $this->wu;
    }

    public function setWu(?string $wu): self
    {
        $this->wu = $wu;

        return $this;
    }

    public function getRia(): ?string
    {
        return $this->ria;
    }

    public function setRia(?string $ria): self
    {
        $this->ria = $ria;

        return $this;
    }


}
