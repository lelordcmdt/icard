<?php

namespace App\Entity;

use App\Repository\CofferStateRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CofferStateRepository::class)
 */
class CofferState
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $orange;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $moov;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $mtn;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $wizall;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $wave;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $cash;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $currency;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $other;
    
 
    /**
     * @ORM\Column(type="date")
     */
    private $dateState;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="cofferStates")
     */
    private $user;
   


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrange(): ?string
    {
        return $this->orange;
    }

    public function setOrange(string $orange): self
    {
        $this->orange = $orange;

        return $this;
    }

    public function getMoov(): ?string
    {
        return $this->moov;
    }

    public function setMoov(string $moov): self
    {
        $this->moov = $moov;

        return $this;
    }

    public function getMtn(): ?string
    {
        return $this->mtn;
    }

    public function setMtn(string $mtn): self
    {
        $this->mtn = $mtn;

        return $this;
    }

    public function getWizall(): ?string
    {
        return $this->wizall;
    }

    public function setWizall(string $wizall): self
    {
        $this->wizall = $wizall;

        return $this;
    }

    public function getWave(): ?string
    {
        return $this->wave;
    }

    public function setWave(string $wave): self
    {
        $this->wave = $wave;

        return $this;
    }

    public function getCash(): ?string
    {
        return $this->cash;
    }

    public function setCash(string $cash): self
    {
        $this->cash = $cash;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getOther(): ?string
    {
        return $this->other;
    }

    public function setOther(string $other): self
    {
        $this->other = $other;

        return $this;
    }
    
    public function getDateState() {
        return $this->dateState;
    }

    public function getUser() {
        return $this->user;
    }

    public function setDateState($dateState): void {
        $this->dateState = $dateState;
    }

    public function setUser($user): void {
        $this->user = $user;
    }


}
