<?php

namespace App\Entity;

use App\Repository\AgencyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AgencyRepository::class)
 */
class Agency
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $manager;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="agency")
     */
    private $users;
   
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CardAgency", mappedBy="agency")
     */
    private $agencyCards;
    
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getManager(): ?string
    {
        return $this->manager;
    }

    public function setManager(string $manager): self
    {
        $this->manager = $manager;

        return $this;
    }
    
    public function getUsers() {
        return $this->users;
    }

    public function setUsers($users): void {
        $this->users = $users;
    }
    
    public function getAgencyCards() {
        return $this->agencyCards;
    }

    public function setAgencyCards($agencyCards): void {
        $this->agencyCards = $agencyCards;
    }

}
