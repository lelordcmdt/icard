<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
     
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder){
        $this->passwordEncoder = $passwordEncoder;
    }
    
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        $user = new User();
        $user->setEmail("m0deste@live.fr");
        $user->setRoles(["ROLE_ADMIN"]);
        $user->setFirstName("Modeste");
        $user->setLastName("KOUASSI");
        $user->setPhone("+225 09 68 38 28");
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            '01020304'
        ));
        $manager->persist($user);
        $manager->flush();
    }
}
