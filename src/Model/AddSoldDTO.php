<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

/**
 * Description of AddSoldDTO
 *
 * @author modestekouassi
 */
class AddSoldDTO {
    //put your code here
    private $soldUbaCI;
    private $soldUbaBF;
    
    public function getSoldUbaCI() {
        return $this->soldUbaCI;
    }

    public function getSoldUbaBF() {
        return $this->soldUbaBF;
    }

    public function setSoldUbaCI($soldUbaCI): void {
        $this->soldUbaCI = $soldUbaCI;
    }

    public function setSoldUbaBF($soldUbaBF): void {
        $this->soldUbaBF = $soldUbaBF;
    }


}
