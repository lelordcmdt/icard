<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

/**
 * Description of PointDTO
 *
 * @author modestekouassi
 */
class PointDTO {
    //put your code here
    private $libelle;
    private $amount;
    
    public function getLibelle() {
        return $this->libelle;
    }

    public function getAmount() {
        return $this->amount;
    }

    public function setLibelle($libelle): void {
        $this->libelle = $libelle;
    }

    public function setAmount($amount): void {
        $this->amount = $amount;
    }

    public function __construct($libelle, $amount) {
        $this->libelle = $libelle;
        $this->amount = $amount;
    }

}
