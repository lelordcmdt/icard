<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

/**
 * Description of ComissionDTO
 *
 * @author modestekouassi
 */
class ComissionDTO {
    //put your code here
    private $agency;
    private $libelle;
    private $amount;
    
    public function getAgency() {
        return $this->agency;
    }

    public function getLibelle() {
        return $this->libelle;
    }

    public function getAmount() {
        return $this->amount;
    }

    public function setAgency($agency): void {
        $this->agency = $agency;
    }

    public function setLibelle($libelle): void {
        $this->libelle = $libelle;
    }

    public function setAmount($amount): void {
        $this->amount = $amount;
    }

    public function __construct($agency, $libelle, $amount) {
        $this->agency = $agency;
        $this->libelle = $libelle;
        $this->amount = $amount;
    }

}
