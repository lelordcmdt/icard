<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

/**
 * Description of CompensationDTO
 *
 * @author modestekouassi
 */
class CompensationDTO {
    //put your code here
    private $agency;
    private $service;
    private $deposit;
    private $withdrawal;  
             

    public function getAgency() {
        return $this->agency;
    }

    public function getService() {
        return $this->service;
    }

    public function getDeposit() {
        return $this->deposit;
    }

    public function getWithdrawal() {
        return $this->withdrawal;
    }

    public function setAgency($agency): void {
        $this->agency = $agency;
    }

    public function setService($service): void {
        $this->service = $service;
    }

    public function setDeposit($deposit): void {
        $this->deposit = $deposit;
    }

    public function setWithdrawal($withdrawal): void {
        $this->withdrawal = $withdrawal;
    }

    public function __construct($agency, $service, $deposit, $withdrawal) {
        $this->agency = $agency;
        $this->service = $service;
        $this->deposit = $deposit;
        $this->withdrawal = $withdrawal;
    }

}
