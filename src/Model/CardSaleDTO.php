<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;
use App\Entity\AfriCard;
/**
 * Description of CardSaleDTO
 *
 * @author modestekouassi
 */
class CardSaleDTO {
    //put your code here
    private $quantity;
    private $clientName;
    private $cardType;
    
    public function getQuantity() {
        return $this->quantity;
    }

    public function getClientName() {
        return $this->clientName;
    }

    public function getCardType() {
        return $this->cardType;
    }

    public function setQuantity($quantity): void {
        $this->quantity = $quantity;
    }

    public function setClientName($clientName): void {
        $this->clientName = $clientName;
    }

    public function setCardType(AfriCard $cardType): void {
        $this->cardType = $cardType;
    }

    
}
