<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Model;

//use App\Entity\Agency;
/**
 * Description of SearchDTO
 *
 * @author modestekouassi
 */
class SearchDTO {
    //put your code here
    private $startDate;
    private $endDate;
    private $occasion;
    private $agency;
    
    public function getStartDate() {
        return $this->startDate;
    }

    public function getEndDate() {
        return $this->endDate;
    }

    public function getOccasion() {
        return $this->occasion;
    }

    public function getAgency() {
        return $this->agency;
    }

    public function setStartDate($startDate): void {
        $this->startDate = $startDate;
    }

    public function setEndDate($endDate): void {
        $this->endDate = $endDate;
    }

    public function setOccasion($occasion): void {
        $this->occasion = $occasion;
    }

    public function setAgency(\App\Entity\Agency $agency): void {
        $this->agency = $agency;
    }


}
