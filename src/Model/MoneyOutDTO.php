<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

/**
 * Description of MoneyOutType
 *
 * @author modestekouassi
 */
class MoneyOutDTO {
    //put your code here
    private $amount;
    private $libelle;
   
    public function getAmount() {
        return $this->amount;
    }

    public function getLibelle() {
        return $this->libelle;
    }


    public function setAmount($amount): void {
        $this->amount = $amount;
    }

    public function setLibelle($libelle): void {
        $this->libelle = $libelle;
    }



}
