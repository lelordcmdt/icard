<?php

namespace App\Form;

use App\Entity\ExtraCash;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class ExtraCashType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('safe', IntegerType::class, ['required' => true])
            ->add('balanceUV', IntegerType::class, ['required' => true])
            ->add('currency', IntegerType::class, ['required' => true])
            ->add('debt', IntegerType::class, ['required' => true])
            ->add('canal', IntegerType::class, ['required' => true])
            ->add('other', IntegerType::class, ['required' => true])
            ->add('misc', IntegerType::class, ['required' => true])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ExtraCash::class,
        ]);
    }
}
