<?php

namespace App\Form;

use App\Entity\Benefit;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class BenefitType extends AbstractType {
    //OM – MTN - MOOV-WIZALL-CANAL+ ["Orange", "Moov", "MTN", "Wizall", "Canal+"]
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('orange', IntegerType::class, ['required' => true])
                ->add('mtn', IntegerType::class, ['required' => true])
                ->add('moov', IntegerType::class, ['required' => true])
                ->add('wizall', IntegerType::class, ['required' => true])
                ->add('canalPlus', IntegerType::class, ['required' => true])
                ->add('africard', IntegerType::class, ['required' => true])
                ->add('moneygram', IntegerType::class, ['required' => true])
                ->add('wu', IntegerType::class, ['required' => true])
                ->add('ria', IntegerType::class, ['required' => true])
                /*->add('type', ChoiceType::class, [
                    'choices' => [
                        'Choisir...' => null,
                        'Orange Money' => Utils::$TYPE_BENEFIT[0],
                        'MTN Money' => Utils::$TYPE_BENEFIT[2],
                        'Moov Money' => Utils::$TYPE_BENEFIT[1],
                        'Wizall' => Utils::$TYPE_BENEFIT[3],
                        'Canal+' => Utils::$TYPE_BENEFIT[4],
                    ],
                ])*/
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Benefit::class,
        ]);
    }

}
