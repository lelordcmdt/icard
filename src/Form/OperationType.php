<?php

namespace App\Form;

use App\Entity\Operation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Occasion;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use App\Repository\OccasionRepository;
use App\Helper\Utils;

class OperationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('typeOperation')
            ->add('amount', IntegerType::class, ['required' => true])
            //->add('dateOperation')
            ->add('wording', TextType::class)
            ->add('occasion', EntityType::class, [
                'required' => true,
                'empty_data' => null,
                'class' => Occasion::class,
                'query_builder' => function (OccasionRepository $er) {
                    return $er->createQueryBuilder('o')
                           ->join('o.heading', 'h')
                           ->andWhere('h.name = :name')
                           ->orderBy('o.name', 'ASC')
                           ->setParameters(['name' => Utils::$SERVICES[0]]);
                },
                'choice_label' => 'name'
            ])
            ->add('comment', TextareaType::class, ['required' => false])
            //->add('user')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Operation::class,
        ]);
    }
}
