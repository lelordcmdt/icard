<?php

namespace App\Form;

use App\Entity\Occasion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\Helper\Utils;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Heading;
//php bin/console make:migration 
class OccasionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['required' => true])
            ->add('typeOperation', ChoiceType::class, ['required' => true, 'choices' => ['Choisir...' => null, 'Depôt' => Utils::$OP_CREDIT, 'Retrait' => Utils::$OP_DEBIT]]) 
            ->add('heading', EntityType::class, [
                'required' => true,
                'empty_data' => null,
                'class' => Heading::class,
                'choice_label' => 'name'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Occasion::class,
        ]);
    }
}
