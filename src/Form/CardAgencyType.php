<?php

namespace App\Form;

use App\Entity\CardAgency;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use App\Entity\AfriCard;
use App\Entity\Agency;

class CardAgencyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quantity', IntegerType::class, ['required' => true])
            ->add('card', EntityType::class, [
                'required' => true,
                'empty_data' => null,
                'class' => AfriCard::class,
                'choice_label' => 'typeCard'
            ])
            ->add('agency', EntityType::class, [
                'required' => true,
                'empty_data' => null,
                'class' => Agency::class,
                'choice_label' => 'name'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CardAgency::class,
        ]);
    }
}
