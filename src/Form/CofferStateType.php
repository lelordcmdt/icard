<?php

namespace App\Form;

use App\Entity\CofferState;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class CofferStateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('orange', IntegerType::class, ['required' => true])
            ->add('moov', IntegerType::class, ['required' => true])
            ->add('mtn', IntegerType::class, ['required' => true])
            ->add('wizall', IntegerType::class, ['required' => true])
            ->add('wave', IntegerType::class, ['required' => true])
            ->add('cash', IntegerType::class, ['required' => true])
            ->add('currency', IntegerType::class, ['required' => true])
            ->add('other', IntegerType::class, ['required' => false])
           // ->add('africard', IntegerType::class, ['required' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CofferState::class,
        ]);
    }
}
