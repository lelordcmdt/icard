<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Model\SearchDTO;
use App\Entity\Occasion;
use App\Entity\Agency;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use App\Repository\OccasionRepository;
use App\Helper\Utils;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Description of SearchFormType
 *
 * @author modestekouassi
 */
class SearchType extends AbstractType {
    //put your code here
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        //
        if($options['index'] == 0) {
            $builder
                ->add('startDate', DateType::class, ['required' => false, 'data' => new \DateTime(), 'empty_data' => (new \DateTime())->format('Y-m-d')])
                ->add('endDate', DateType::class, ['required' => false, 'data' => new \DateTime(),
                'empty_data' => (new \DateTime())->format('Y-m-d')])
                ->add('occasion', EntityType::class, [
                    'required' => false,
                    'empty_data' => null,
                    'placeholder' => 'Choisir...',
                    'class' => Occasion::class,
                    'query_builder' => function (OccasionRepository $er) {
                        return $er->createQueryBuilder('o')
                               ->join('o.heading', 'h')
                               ->andWhere('h.name = :name_one OR h.name = :name_two OR h.name = :name_three')
                               ->orderBy('o.name', 'ASC')
                               ->setParameters(['name_one' => Utils::$SERVICES[0], 'name_two' => Utils::$SERVICES[1], 'name_three' => Utils::$SERVICES[2]]);
                    },
                    'choice_label' => 'name'
                ])
                ->add('agency', EntityType::class, [
                    'required' => false,
                    'empty_data' => null,
                    'placeholder' => 'Choisir...',
                    'class' => Agency::class,
                    'choice_label' => 'name'
                ])
            ;
        }
        
        if($options['index'] == 1) {
            $builder
                ->add('startDate', DateType::class, ['required' => false, 'data' => new \DateTime(), 'empty_data' => (new \DateTime())->format('Y-m-d')])
                ->add('endDate', DateType::class, ['required' => false, 'data' => new \DateTime(), 'empty_data' => (new \DateTime())->format('Y-m-d')])
                ->add('occasion', ChoiceType::class, ['required' => false, 'choices' => [
                    'Choisir...' => null,
                    Utils::$SERVICES[4] => Utils::$SERVICES[4], 
                    Utils::$SERVICES[5] => Utils::$SERVICES[5],
                    Utils::$SERVICES[6] => Utils::$SERVICES[6], 
                    Utils::$SERVICES[7] => Utils::$SERVICES[7]
                ]])
                ->add('agency', EntityType::class, [
                    'required' => false,
                    'empty_data' => null,
                    'placeholder' => 'Choisir...',
                    'class' => Agency::class,
                    'choice_label' => 'name'
                ])
            ;
        }
        
        if($options['index'] == 2) {
            $builder
                ->add('startDate', DateType::class, ['required' => false, 'data' => new \DateTime(), 'empty_data' =>  (new \DateTime())->format('Y-m-d')])
                ->add('endDate', DateType::class, ['required' => false, 'data' => new \DateTime(), 'empty_data' => (new \DateTime())->format('Y-m-d')])
                ->add('occasion', ChoiceType::class, ['required' => true, 'choices' => [
                    'Choisir...' => null,
                    'Werstern Union' => 'Werstern Union', 
                    'Money-Gram' => 'Money-Gram', 
                    'Ria' => 'Ria'
                ]])
                ->add('agency', EntityType::class, [
                    'required' => false,
                    'placeholder' => 'Choisir...',
                    'empty_data' => null,
                    'class' => Agency::class,
                    'choice_label' => 'name'
                ])
            ;
        }
        
        if($options['index'] == 3) {
            $builder
                ->add('startDate', DateType::class, ['required' => false, 'data' => new \DateTime(), 'empty_data' =>  (new \DateTime())->format('Y-m-d')])
                ->add('endDate', DateType::class, ['required' => false, 'data' => new \DateTime(), 'empty_data' => (new \DateTime())->format('Y-m-d')])
                ->add('occasion', ChoiceType::class, ['required' => false, 'choices' => [
                    'Choisir...' => null,
                    Utils::$SERVICES[4] => Utils::$SERVICES[4], 
                    Utils::$SERVICES[5] => Utils::$SERVICES[5]
                ]])
                ->add('agency', EntityType::class, [
                    'required' => false,
                    'empty_data' => null,
                    'placeholder' => 'Choisir...',
                    'class' => Agency::class,
                    'choice_label' => 'name'
                ])
            ;
        }
        
       
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SearchDTO::class,
        ]);
        $resolver->setRequired(array(
            'index'
        ));
    }
}
