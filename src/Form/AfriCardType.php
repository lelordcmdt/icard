<?php

namespace App\Form;

use App\Entity\AfriCard;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Helper\Utils;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class AfriCardType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('typeCard', ChoiceType::class, ['required' => true, 'choices' => ['Choisir...' => null, Utils::$CARD_UBA_CI => Utils::$CARD_UBA_CI, Utils::$CARD_UBA_BF => Utils::$CARD_UBA_BF]])
            ->add('price', IntegerType::class, ['required' => true])
            ->add('buyPrice', IntegerType::class, ['required' => true])
            ->add('quantity', IntegerType::class, ['required' => true])
            ->add('benefit', IntegerType::class, ['required' => true])
            ->add('benefitPerCent', IntegerType::class, ['required' => true])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AfriCard::class,
        ]);
    }
}
