<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use App\Model\AddSoldDTO;


/**
 * Description of AddCardSoldType
 *
 * @author modestekouassi
 */
class AddCardSoldType extends AbstractType {
    //put your code here
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('soldUbaCI', IntegerType::class, ['required' => true])
            ->add('soldUbaBF', IntegerType::class, ['required' => true])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AddSoldDTO::class,
        ]);
    }
}
