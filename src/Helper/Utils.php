<?php
namespace App\Helper;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Utils
 *
 * @author modestekouassi
 */
class Utils {
    //put your code here
    public static  $OP_DEBIT = "Débit";
    public static  $OP_CREDIT = "Crédit";
    
    public static  $CARD_UBA_CI = "UBA CI";
    public static  $CARD_UBA_BF = "UBA BF";
    
    public static  $TYPE_BENEFIT = ["Orange", "Moov", "MTN", "Wizall", "Canal+"];
    public static  $SERVICES = [
        "Vente", //0
        "Dépense", //1
        "Achat",  //2
        "Banque",  //3
        "Rechargement de carte UBA CI",  //4 
        "Rechargement de carte UBA BF",  //5
        "Vente de carte UBA CI",  //6
        "Vente de carte UBA BF",  //7
        "Dépôt Ria",  //8
        "Dépôt Werstern Union",  //9
        "Dépôt Money-Gram",  //10
        "Retrait Ria",  //11
        "Retrait Werstern Union",  //12
        "Retrait Money-Gram" //13
        ];
}
