<?php

namespace App\Controller;

use App\Entity\Benefit;
use App\Form\BenefitType;
use App\Repository\BenefitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\SearchDTO;
use App\Form\SearchType;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * @Route("/benefit")
 */
class BenefitController extends AbstractController
{
    
    private $session;

    public function __construct(SessionInterface $session) {
        $this->session = $session;
    }
    
    
    /**
     * @Route("/{type}/list", name="benefit_index", methods={"GET", "POST"})
     */
    public function index(String $type, BenefitRepository $benefitRepository, Request $request): Response
    {
        $model = new SearchDTO();
        $form = $this->createForm(SearchType::class, $model, ['index' => 2]);
        $form->handleRequest($request);
        
        //$list = $benefitRepository->findByDate(($this->session->get('currentDate'))->format('Y-m-d H:i:s'));
        $list = $benefitRepository->findAll();
        
        if($type == "two") {
            //LIST DE TOUS LES AGENTS
            $list = $benefitRepository->findByRole("ROLE_USER");
            
        } elseif($type == "one") {
            //LIST DE L'AGENT PAR DATE
            $list = $benefitRepository->findByDateAndUser($this->getUser(), $this->session->get('currentDate')->format('Y-m-d H:i:s'));
            
        }elseif($type == "three") {
             //LIST DU MANAGER
             $list = $benefitRepository->findByRole("ROLE_ADMIN"); 
        }
        else {
            
            if($this->isGranted("ROLE_ADMIN")) {
                 //LIST DU MANAGER
                $list = $benefitRepository->findByRole("ROLE_ADMIN"); 
                $type = "three";
            } else {
                  //LIST DE L'AGENT PAR DATE
                $list = $benefitRepository->findByDateAndUser($this->getUser(), $this->session->get('currentDate')->format('Y-m-d H:i:s'));
                $type = "one";
            }
        }
        
        if ($form->isSubmitted() && $form->isValid()) {
            
            if($this->isGranted("ROLE_ADMIN")) {
                if($model->getAgency() != null) 
                    //1
                    $list = $benefitRepository->findBetweenTwoDateAndUser($model->getAgency()->getUsers()[0], $model->getStartDate(), $model->getEndDate(), "ROLE_USER");
                else 
                    //2
                    $list = $benefitRepository->findBetweenTwoDate($model->getStartDate(), $model->getEndDate(), "ROLE_ADMIN");
                
            }

        }
       
        return $this->render('benefit/index.html.twig', [
            'benefits' => $list,
            'type' => $type,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="benefit_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $benefit = new Benefit();
        $form = $this->createForm(BenefitType::class, $benefit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $benefit->setDateBen(new \DateTime());
            $benefit->setUser($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($benefit);
            $entityManager->flush();

            return $this->redirectToRoute('benefit_index', ['type' => "four"]);
        }

        return $this->render('benefit/new.html.twig', [
            'benefit' => $benefit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="benefit_show", methods={"GET"})
     */
    public function show(Benefit $benefit): Response
    {
        return $this->render('benefit/show.html.twig', [
            'benefit' => $benefit,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="benefit_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Benefit $benefit): Response
    {
        $form = $this->createForm(BenefitType::class, $benefit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('benefit_index');
        }

        return $this->render('benefit/edit.html.twig', [
            'benefit' => $benefit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="benefit_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Benefit $benefit): Response
    {
        if ($this->isCsrfTokenValid('delete'.$benefit->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($benefit);
            $entityManager->flush();
        }

        return $this->redirectToRoute('benefit_index');
    }
}
