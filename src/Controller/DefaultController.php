<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//https://medium.com/@runawaycoin/deploying-symfony-4-application-to-shared-hosting-with-just-ftp-access-e65d2c5e0e3d
namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\CardAgencyRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Repository\AfriCardRepository;
use App\Model\AddSoldDTO;
use App\Model\MoneyOutDTO;
use App\Form\AddCardSoldType;
use App\Helper\Utils;
use App\Form\MoneyOutType;
use App\Repository\OccasionRepository;
use App\Repository\OperationRepository;
use App\Model\SearchDTO;
use App\Form\SearchType;
use App\Entity\Operation;
use App\Repository\AgencyRepository;
use App\Model\PointDTO;
use App\Repository\ExtraCashRepository;

/**
 * Description of DefaultController
 *
 * @author modestekouassi
 */
class DefaultController extends AbstractController {
    //put your code here
    
    private $session;

    public function __construct(SessionInterface $session) {
        $this->session = $session;
    }
    
    /**
     * @Route("/admin", name="admin_dashboard", methods={"GET"})
     */
    public function dashboard(): Response
    {
        return $this->render('default/dashboard.html.twig');
    }
    
    /**
     * @Route("/estate", name="estate", methods={"GET"})
     */
    public function estate(ExtraCashRepository $extraCashRepository, OperationRepository $operationRepository, AfriCardRepository $afriCardRepository, AgencyRepository $agencyRepository, OccasionRepository $occasionRepository): Response
    {
        
        $ubaci = $afriCardRepository->findOneBy(["typeCard" => Utils::$CARD_UBA_CI]);
        $ubabf = $afriCardRepository->findOneBy(["typeCard" => Utils::$CARD_UBA_BF]);
            
        //SOLDE CAISSE AGENT
        $checkout = array();
        foreach($agencyRepository->findAll() as $agency) {
            $coffer = $operationRepository->countByTypeOperation($agency->getUsers()[0], Utils::$OP_DEBIT, ($this->session->get('currentDate'))->format('Y-m-d H:i:s'))[0]["credit"] - $operationRepository->countByUserAndDebit($agency->getUsers()[0], Utils::$OP_DEBIT, ($this->session->get('currentDate'))->format('Y-m-d H:i:s'))[0]["debit"];
            array_push($checkout, new PointDTO($agency->getName(), $coffer));
        }
        
        
        //SOLDE AFRICARD
        $card = array();
         //var_dump($amountubabf);
        array_push($card, new PointDTO("UBA CI", $ubaci->getSold()), new PointDTO("UBA BF", $ubabf->getSold()));
       
           
        //COMPTE BENEFICE
        $benefit = array();
        $amountubaci = $operationRepository->countByCards($occasionRepository->findOneBy(['name' => Utils::$SERVICES[6]]))[0]["totalcredit"];
        $amountubabf = $operationRepository->countByCards($occasionRepository->findOneBy(['name' => Utils::$SERVICES[7]]))[0]["totalcredit"];
       
        $rechargementci = $operationRepository->findByOccasion($occasionRepository->findOneBy(['name' => Utils::$SERVICES[4]]));
        $rechargementbf = $operationRepository->findByOccasion($occasionRepository->findOneBy(['name' => Utils::$SERVICES[5]]));

        $totalci = 0;
        foreach($rechargementci as $item) {

            if($item->getAmount() > 75000){
               $totalci = $totalci + ($item->getAmount() * 0.02) * ($ubaci->getBenefitPerCent()/100); 
            } else {
               $totalci = $totalci + 400;
            }

        }

        $totalbf = 0;
        foreach($rechargementbf as $item) {
            if($item->getAmount() > 45000){
               $totalbf = $totalbf + ($item->getAmount() * 0.02) * ($ubabf->getBenefitPerCent()/100); 
            } else {
               $totalbf = $totalbf + 600;
            }
        }
        
       // $olddiff = $operationRepository->countByCreditAndOccasion($occasionRepository->findOneBy(['name' => Utils::$SERVICES[1]]), Utils::$OP_DEBIT, ($this->session->get('currentDate'))->format('Y-m-d'))[0]["totalcredit"] - $operationRepository->countByDebitAndOccasion($occasionRepository->findOneBy(['name' => Utils::$SERVICES[1]]), Utils::$OP_DEBIT, ($this->session->get('currentDate'))->format('Y-m-d'))[0]["totaldebit"];
       // $oldexpensebalance = (($ubabf->getPrice() - $ubabf->getBuyPrice()) * ($amountubabf / $ubabf->getPrice())) + (($ubaci->getPrice() - $ubaci->getBuyPrice()) * ($amountubaci / $ubaci->getPrice())) + $totalci + $totalbf + $diff;
        
        
        $alldiff = $operationRepository->countByCreditAndOccasionAll($occasionRepository->findOneBy(['name' => Utils::$SERVICES[1]]), Utils::$OP_DEBIT)[0]["totalcredit"] - $operationRepository->countByDebitAndOccasionAll($occasionRepository->findOneBy(['name' => Utils::$SERVICES[1]]), Utils::$OP_DEBIT)[0]["totaldebit"];
        
        $newexpensebalance = (($ubabf->getPrice() - $ubabf->getBuyPrice()) * ($amountubabf / $ubabf->getPrice())) + (($ubaci->getPrice() - $ubaci->getBuyPrice()) * ($amountubaci / $ubaci->getPrice())) + $totalci + $totalbf + $alldiff;
        
        $today_expenses = $operationRepository->findByDateAndOccasionSUM(new \DateTime(), $occasionRepository->findOneBy(['name' => Utils::$SERVICES[1]]))[0]["total"] + 0;
        
        array_push($benefit, new PointDTO("Dépense du jour", $today_expenses), new PointDTO("Nouveau solde compte dépense", $newexpensebalance));
       
        //$commissions = 0;
        //$depenses = $operationRepository->countByDebitAndOccasion($occasionRepository->findOneBy(['name' => Utils::$SERVICES[1]]), Utils::$OP_DEBIT, ($this->session->get('currentDate'))->format('Y-m-d'))[0]["totaldebit"];
        
        
        
        //SOLDE BANK
        $bank = array();
        
        $sold = $operationRepository->countByOccasion(Utils::$SERVICES[3], Utils::$OP_CREDIT)[0]["total"] - $operationRepository->countByOccasion(Utils::$SERVICES[3], Utils::$OP_DEBIT)[0]["total"];

        $envoieria = $operationRepository->countByOccasion(Utils::$SERVICES[8], Utils::$OP_CREDIT)[0]["total"];
        $envoiewu = $operationRepository->countByOccasion(Utils::$SERVICES[9], Utils::$OP_CREDIT)[0]["total"];
        $envoiemoneygram = $operationRepository->countByOccasion(Utils::$SERVICES[10], Utils::$OP_CREDIT)[0]["total"];
        //=====================================
        $retraitria = $operationRepository->countByOccasion(Utils::$SERVICES[11], Utils::$OP_DEBIT)[0]["total"];
        $retraitwu = $operationRepository->countByOccasion(Utils::$SERVICES[12], Utils::$OP_DEBIT)[0]["total"];
        $retraitmoneygram = $operationRepository->countByOccasion(Utils::$SERVICES[13], Utils::$OP_DEBIT)[0]["total"];

        $totalenvoie = $envoieria + $envoiewu + $envoiemoneygram;
        $totalretrait = $retraitria + $retraitwu + $retraitmoneygram;
        $compense = $totalenvoie - $totalretrait;

        $totalbank = $compense + $sold;
        
        array_push($bank, new PointDTO("Solde antérieur", $sold)
                , new PointDTO("Compensation du jour", $compense)/*
                , new PointDTO("Nouveau solde compte Banque", $totalbank)*/);
       
        
        //SOLDE EXTRA
        $extra = array();
        $data = $extraCashRepository->findBy(array(),array('id'=>'DESC'),1,0);
        //var_dump($data);
        if (count($data) > 0) {
            array_push($extra, new PointDTO("Coffre", $data[0]->getSafe())
                  , new PointDTO("Solde UV", $data[0]->getBalanceUV())
                  , new PointDTO("Devise", $data[0]->getCurrency())
                  , new PointDTO("Canal", $data[0]->getCanal())
                  , new PointDTO("Dette", $data[0]->getDebt())
                  , new PointDTO("Autre", $data[0]->getOther())
                  , new PointDTO("Divers", $data[0]->getMisc()));
        }
          
     
          
        return $this->render('default/estate.html.twig', [
            'checkouts' => $checkout,
            'cards' => $card,
            'benefits' => $benefit,
            'banks' => $bank,
            'extras' => $extra,
        ]);
        
    }
    
    /**
     * @Route("/compensations/{type}", name="compensations", methods={"GET","POST"})
     */
    public function compensation(String $type, OperationRepository $operationRepository, Request $request): Response
    {
         $compensations = $operationRepository->findAll();
         
        $model = new SearchDTO();
        $form = $this->createForm(SearchType::class, $model, ['index' => 2]);
        if($type == "cashin") {
            
            $form = $this->createForm(SearchType::class, $model, ['index' => 3]);
            $compensations = $operationRepository->findByUserAndOccasion(null, Utils::$SERVICES[4], Utils::$OP_CREDIT);
            $compensations += $operationRepository->findByUserAndOccasion(null, Utils::$SERVICES[5], Utils::$OP_CREDIT);
        
        } else {
            
            $compensations = $operationRepository->findByUserAndOccasion(null, Utils::$SERVICES[8], Utils::$OP_CREDIT);
            $compensations += $operationRepository->findByUserAndOccasion(null, Utils::$SERVICES[9], Utils::$OP_CREDIT);
            $compensations += $operationRepository->findByUserAndOccasion(null, Utils::$SERVICES[10], Utils::$OP_CREDIT);
             //=====================================
            $compensations += $operationRepository->findByUserAndOccasion(null, Utils::$SERVICES[11], Utils::$OP_DEBIT);
            $compensations += $operationRepository->findByUserAndOccasion(null, Utils::$SERVICES[12], Utils::$OP_DEBIT);
            $compensations += $operationRepository->findByUserAndOccasion(null, Utils::$SERVICES[13], Utils::$OP_DEBIT);

        }
           
        $form->handleRequest($request);
       
      
        if ($form->isSubmitted() && $form->isValid()) {
            
            if($this->isGranted("ROLE_ADMIN")) {
                if($model->getAgency() != null) {
                    if($model->getOccasion() != null) {
                        //1
                        $compensations = $operationRepository->findBetweenDateAndOccasionAndUser($model->getAgency()->getUsers()[0], $model->getOccasion(), $model->getStartDate(), $model->getEndDate());
       
                    } else {
                        //2
                        $compensations = $operationRepository->findBetweenDateAndUser($model->getAgency()->getUsers()[0], $model->getStartDate(), $model->getEndDate());
       
                    }
                } else {
                    if($model->getOccasion() != null) {
                        //3
                        $compensations = $operationRepository->findBetweenDateAndOccasion($model->getOccasion(), $model->getStartDate(), $model->getEndDate());
       
                    } else {
                        //4
                        $compensations = $operationRepository->findBetweenDate($model->getStartDate(), $model->getEndDate());
       
                    }
                }
            } 
        }
           
        return $this->render('default/compensation.html.twig', [
            'operations' => $compensations,
            'type' => Utils::$OP_DEBIT,
            'form' => $form->createView(),
        ]);
    }
    
    
   
    
    /**
     * @Route("/commissions", name="commissions", methods={"GET","POST"})
     */
    public function commissions(Request $request, AfriCardRepository $afriCardRepository, OperationRepository $operationRepository, OccasionRepository $occasionRepository): Response
    {
        $model = new SearchDTO();
        $form = $this->createForm(SearchType::class, $model, ['index' => 1]);
        $form->handleRequest($request);
       
        
        $ubaci = $afriCardRepository->findOneBy(["typeCard" => Utils::$CARD_UBA_CI]);
        $ubabf = $afriCardRepository->findOneBy(["typeCard" => Utils::$CARD_UBA_BF]);
        
      
        //$commissions = array();
        $commissions = $operationRepository->findCards($occasionRepository->findOneBy(['name' => Utils::$SERVICES[6]]));

        $commissions += $operationRepository->findCards($occasionRepository->findOneBy(['name' => Utils::$SERVICES[7]]));

        $commissions += $operationRepository->findByUserAndOccasion(null, $occasionRepository->findOneBy(['name' => Utils::$SERVICES[4]]));
        $commissions += $operationRepository->findByUserAndOccasion(null, $occasionRepository->findOneBy(['name' => Utils::$SERVICES[5]]));

        if ($form->isSubmitted() && $form->isValid()) {
            //echo '<script>alert('.$model->getOccasion().');</script>';
             
            if($this->isGranted("ROLE_ADMIN")) {
                if($model->getAgency() != null) {
                    if($model->getOccasion() != null) {
                        //1
                        $occasion = $occasionRepository->findOneBy(["name" => $model->getOccasion()]);
                        $commissions = $operationRepository->findBetweenDateAndOccasionAndUser($model->getAgency()->getUsers()[0], $occasion, $model->getStartDate(), $model->getEndDate());
       
                    } else {
                        //2
                        $commissions = $operationRepository->findBetweenDateAndUser($model->getAgency()->getUsers()[0], $model->getStartDate(), $model->getEndDate());
       
                    }
                } else {
                    if($model->getOccasion() != null) {
                        //3
                        $occasion = $occasionRepository->findOneBy(["name" => $model->getOccasion()]);
                        $commissions = $operationRepository->findBetweenDateAndOccasion($occasion, $model->getStartDate(), $model->getEndDate());
       
                    } else {
                        //4
                        $commissions = $operationRepository->findBetweenDate($model->getStartDate(), $model->getEndDate());
       
                    }
                }
            } 
        }
        
        return $this->render('default/commission.html.twig', [
            'operations' => $commissions,
            'ubaci' => $ubaci,
            'ubabf' => $ubabf,
            'form' => $form->createView(),
            'type' => Utils::$OP_DEBIT
        ]);
    }
    
    
    
    /**
     * @Route("/inventory", name="inventory_index", methods={"GET"})
     */
    public function inventaire(AfriCardRepository $afriCardRepository, CardAgencyRepository $cardAgencyRepository, OperationRepository $operationRepository): Response
    {
        
        //OperationRepository $operationRepository,  
        $cards = $cardAgencyRepository->findAll();
        $ops = $operationRepository->findByDateAndUser($this->getUser(), ($this->session->get('currentDate'))->format('Y-m-d H:i:s'));
        
        $ci_buy = 0;
        $bf_buy = 0;
        foreach ($ops as $op){
            
            if ($op->getOccasion()->getName() == 'Vente de carte UBA CI'){
                $card = $afriCardRepository->findOneBy(["typeCard" => Utils::$CARD_UBA_CI]);
                $ci_buy = $ci_buy + ($op->getAmount() / $card->getPrice());
            } 
            
            if ($op->getOccasion()->getName() == 'Vente de carte UBA BF'){
                $card = $afriCardRepository->findOneBy(["typeCard" => Utils::$CARD_UBA_BF]);
                $bf_buy = $bf_buy + ($op->getAmount() / $card->getPrice());
            } 
  
        }
        
        $list = array();
        foreach ($cards as $card){
            
            if($card->getUser() != null)
                if($card->getUser()->getId() == $this->getUser()->getId())
                    array_push($list, $card);
            
                
        }
        
        //echo "<script>alert('".count($list)."');</script>";
        //$cards = $cardAgencyRepository->findBy(["agency" => $this->getUser()->getAgency()]);
        
        return $this->render('default/inventaire.html.twig', [
            //'oldlist' => $this->session->get('cards'),
            'buy_uba_ci' => $ci_buy,
            'buy_uba_bf' => $bf_buy,
            'newlist' => $list,
        ]);
        
    }
    
    /**
     * @Route("/sold-cards", name="sold_card_new", methods={"GET","POST"})
     */
    public function soldCardNew(Request $request, AfriCardRepository $afriCardRepository): Response
    {
       
        
        $dto = new AddSoldDTO();
        $form = $this->createForm(AddCardSoldType::class, $dto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $entityManager = $this->getDoctrine()->getManager();
            $ubaci = $afriCardRepository->findOneBy(["typeCard" => Utils::$CARD_UBA_CI]);
            $ubabf = $afriCardRepository->findOneBy(["typeCard" => Utils::$CARD_UBA_BF]);
            
            $ubaci->setSold($dto->getSoldUbaCI());
            $ubabf->setSold($dto->getSoldUbaBF());
            $ubabf->setDateSave(new \DateTime());
            $ubaci->setDateSave(new \DateTime());
            //$dto->setUser($this->getUser());
            
            $entityManager->persist($ubaci);
            $entityManager->persist($ubabf);
             
            $entityManager->flush();

            return $this->redirectToRoute('inventory_index');
        }

        return $this->render('default/sold_card_new.html.twig', [
            'model' => $dto,
            'form' => $form->createView(),
        ]);
    }
    
    
    /**
     * @Route("/money-out/{type}", name="money_out_index", methods={"GET","POST"})
     */
    public function moneyOutIndex(Request $request, OperationRepository $operationRepository, String $type, OccasionRepository $occasionRepository, AfriCardRepository $afriCardRepository): Response
    {
        
        $model = new SearchDTO();
        $form = $this->createForm(SearchType::class, $model, ['index' => 0]);
        $form->handleRequest($request);
        
        $occasion = $occasionRepository->findOneBy(['name' => $type]);
        $list = $operationRepository->findByDateAndHeading(($this->session->get('currentDate'))->format('Y-m-d H:i:s'), $occasion);
        
        
        $coffer = $operationRepository->countByCredit(Utils::$OP_DEBIT, ($this->session->get('currentDate'))->format('Y-m-d H:i:s'))[0]["totalcredit"] - $operationRepository->countByDebit(Utils::$OP_DEBIT, ($this->session->get('currentDate'))->format('Y-m-d H:i:s'))[0]["totaldebit"];
        
        
        if($this->isGranted("ROLE_USER") && !$this->isGranted("ROLE_ADMIN")) {
           $coffer = $operationRepository->countByTypeOperation($this->getUser(), Utils::$OP_DEBIT, ($this->session->get('currentDate'))->format('Y-m-d H:i:s'))[0]["credit"] - $operationRepository->countByUserAndDebit($this->getUser(), Utils::$OP_DEBIT, ($this->session->get('currentDate'))->format('Y-m-d H:i:s'))[0]["debit"];
           $list = $list = $operationRepository->findByDateAndHeadingAndUser(($this->session->get('currentDate'))->format('Y-m-d H:i:s'), $occasion, $this->getUser());
        }
        
        
        if ($type == Utils::$SERVICES[1]) {
            //CALCUL COMMISSION SUR RECHARGEMENT ET VENTE DE CARTE
            $ubaci = $afriCardRepository->findOneBy(["typeCard" => Utils::$CARD_UBA_CI]);
            $ubabf = $afriCardRepository->findOneBy(["typeCard" => Utils::$CARD_UBA_BF]);
            
            
            if($this->isGranted("ROLE_USER") && !$this->isGranted("ROLE_ADMIN")) {
                  
                $amountubaci = $operationRepository->countByUserCard($this->getUser(), $occasionRepository->findOneBy(['name' => Utils::$SERVICES[6]]))[0]["totalcredit"];

                $amountubabf = $operationRepository->countByUserCard($this->getUser(), $occasionRepository->findOneBy(['name' => Utils::$SERVICES[7]]))[0]["totalcredit"];


                $rechargementci = $operationRepository->findByUserAndOccasion($this->getUser(), $occasionRepository->findOneBy(['name' => Utils::$SERVICES[4]]));
                $rechargementbf = $operationRepository->findByUserAndOccasion($this->getUser(), $occasionRepository->findOneBy(['name' => Utils::$SERVICES[5]]));

                $totalci = 0;
                foreach($rechargementci as $item) {

                    if($item->getAmount() > 75000){
                       $totalci = $totalci + ($item->getAmount() * 0.02) * ($ubaci->getBenefitPerCent()/100); 
                    } else {
                       $totalci = $totalci + 400;
                    }

                }

                $totalbf = 0;
                foreach($rechargementbf as $item) {
                    if($item->getAmount() > 45000){
                       $totalbf = $totalbf + ($item->getAmount() * 0.02) * ($ubabf->getBenefitPerCent()/100); 
                    } else {
                       $totalbf = $totalbf + 600;
                    }
                }
                
                //$balance = $operationRepository->countByOccasionAndUser($this->getUser(), Utils::$SERVICES[1], Utils::$OP_CREDIT)[0]["total"] - $operationRepository->countByOccasionAndUser($this->getUser(), Utils::$SERVICES[1], Utils::$OP_DEBIT)[0]["total"];

                //$coffer = $balance + (($ubabf->getPrice() - $ubabf->getBuyPrice()) * ($amountubabf / $ubabf->getPrice())) + + (($ubaci->getPrice() - $ubaci->getBuyPrice()) * ($amountubaci / $ubaci->getPrice())) + $totalci + $totalbf;

                $coffer = (($ubabf->getPrice() - $ubabf->getBuyPrice()) * ($amountubabf / $ubabf->getPrice())) + (($ubaci->getPrice() - $ubaci->getBuyPrice()) * ($amountubaci / $ubaci->getPrice())) + $totalci + $totalbf;

            } else {
                $amountubaci = $operationRepository->countByCard($occasionRepository->findOneBy(['name' => Utils::$SERVICES[6]]))[0]["totalcredit"];
                $amountubabf = $operationRepository->countByCard($occasionRepository->findOneBy(['name' => Utils::$SERVICES[7]]))[0]["totalcredit"];

                $rechargementci =  $operationRepository->findByOccasion($occasionRepository->findOneBy(['name' => Utils::$SERVICES[4]]));
                $rechargementbf =  $operationRepository->findByOccasion($occasionRepository->findOneBy(['name' => Utils::$SERVICES[5]]));

                $totalci = 0;
                
                foreach($rechargementci as $item) {

                    if($item->getAmount() > 75000){
                       $totalci = $totalci + ($item->getAmount() * 0.02) * ($ubaci->getBenefitPerCent()/100); 
                    } else {
                       $totalci = $totalci + 400;
                    }

                }

                $totalbf = 0;
                foreach($rechargementbf as $item) {
                    if($item->getAmount() > 45000){
                       $totalbf = $totalbf + ($item->getAmount() * 0.02) * ($ubabf->getBenefitPerCent()/100); 
                    } else {
                       $totalbf = $totalbf + 600;
                    }
                }

                //$balance = $operationRepository->countByOccasion(Utils::$SERVICES[1], Utils::$OP_CREDIT)[0]["total"] - $operationRepository->countByOccasion(Utils::$SERVICES[1], Utils::$OP_DEBIT)[0]["total"];

                //$coffer = $balance + (($ubabf->getPrice() - $ubabf->getBuyPrice()) * ($amountubabf / $ubabf->getPrice())) + + (($ubaci->getPrice() - $ubaci->getBuyPrice()) * ($amountubaci / $ubaci->getPrice())) + $totalci + $totalbf;

                $coffer = (($ubabf->getPrice() - $ubabf->getBuyPrice()) * ($amountubabf / $ubabf->getPrice())) + (($ubaci->getPrice() - $ubaci->getBuyPrice()) * ($amountubaci / $ubaci->getPrice())) + $totalci + $totalbf;

            }
        }
        
        
        if ($type == Utils::$SERVICES[3]) {
            //CALCUL COMPENSATION
            if($this->isGranted("ROLE_ADMIN")) {
                
                //$balance = $operationRepository->countByOccasion(Utils::$SERVICES[3], Utils::$OP_CREDIT)[0]["total"] - $operationRepository->countByOccasion(Utils::$SERVICES[3], Utils::$OP_DEBIT)[0]["total"];

                $envoieria = $operationRepository->countByOccasion(Utils::$SERVICES[8], Utils::$OP_CREDIT)[0]["total"];
                $envoiewu = $operationRepository->countByOccasion(Utils::$SERVICES[9], Utils::$OP_CREDIT)[0]["total"];
                $envoiemoneygram = $operationRepository->countByOccasion(Utils::$SERVICES[10], Utils::$OP_CREDIT)[0]["total"];
                //=====================================
                $retraitria = $operationRepository->countByOccasion(Utils::$SERVICES[11], Utils::$OP_DEBIT)[0]["total"];
                $retraitwu = $operationRepository->countByOccasion(Utils::$SERVICES[12], Utils::$OP_DEBIT)[0]["total"];
                $retraitmoneygram = $operationRepository->countByOccasion(Utils::$SERVICES[13], Utils::$OP_DEBIT)[0]["total"];
           
                $totalenvoie = $envoieria + $envoiewu + $envoiemoneygram;
                $totalretrait = $retraitria + $retraitwu + $retraitmoneygram;
                $compense = $totalenvoie - $totalretrait;
                
                //$coffer = $balance + $compense;
                $coffer = $compense;
                
            }
         
        }
        
        if ($form->isSubmitted() && $form->isValid()) {
            //echo '<script>alert('.$model->getOccasion().');</script>';
            if($this->isGranted("ROLE_ADMIN")) {
                 
                if($model->getAgency() != null) {
                    //1
                    $list = $operationRepository->findBetweenDateAndOccasionAndUser($model->getAgency()->getUsers()[0], $occasion, $model->getStartDate(), $model->getEndDate());
       
                } else {
                    //2
                    $list = $operationRepository->findBetweenDateAndOccasion($occasion, $model->getStartDate(), $model->getEndDate());
       
                }
                
            } 
        }
        
        return $this->render('default/money_out_index.html.twig', [
            'operations' => $list,
            'type' => $type,
            'form' => $form->createView(),
            'coffer' => $coffer,
        ]);
    }
    
    
    /**
     * @Route("/money-out/new/{type}", name="money_out_new", methods={"GET","POST"})
     */
    public function moneyOutNew(Request $request, String $type, OccasionRepository $occasionRepository): Response
    {
        
        $dto = new MoneyOutDTO();
        $form = $this->createForm(MoneyOutType::class, $dto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $entityManager = $this->getDoctrine()->getManager();
            
            $operation = new Operation();
            $operation->setUser($this->getUser());
            if ($type == Utils::$SERVICES[1]) {
                $operation->setDateOperation(new \DateTime('last month'));
            } else {
                $operation->setDateOperation($this->session->get('currentDate'));
            }
            
            $operation->setAmount($dto->getAmount());
            $operation->setComment($dto->getLibelle());
            $occasion = $occasionRepository->findOneBy(['name' => $type]);
            $operation->setOccasion($occasion);
            $operation->setWording($dto->getLibelle());

            $entityManager->persist($operation);
            $entityManager->flush();

            return $this->redirectToRoute('money_out_index', ["type" => $type]);
        }

        return $this->render('default/money_out_new.html.twig', [
            'model' => $dto,
            'type' => $type,
            'route' => 'money_out_new',
            'form' => $form->createView(),
        ]);
    }
    
    
    /**
     * @Route("/money-in/new/{type}", name="money_in_new", methods={"GET","POST"})
     */
    public function moneyInNew(Request $request, String $type, OccasionRepository $occasionRepository): Response
    {
        
        $dto = new MoneyOutDTO();
        $form = $this->createForm(MoneyOutType::class, $dto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $entityManager = $this->getDoctrine()->getManager();
            
            $operation = new Operation();
            $operation->setUser($this->getUser());
            $operation->setDateOperation($this->session->get('currentDate'));
            $operation->setAmount($dto->getAmount());
            $operation->setComment($dto->getLibelle());
            $occasion = $occasionRepository->findOneBy(['name' => 'Apport '.$type]);
            $operation->setOccasion($occasion);
            $operation->setWording($dto->getLibelle());

            $entityManager->persist($operation);
            $entityManager->flush();

            return $this->redirectToRoute('money_out_index', ["type" => $type]);
        }

        return $this->render('default/money_out_new.html.twig', [
            'model' => $dto,
            'route' => 'money_in_new',
            'type' => $type,
            'form' => $form->createView(),
        ]);
    }
    
    
    
}
