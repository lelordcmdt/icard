<?php

namespace App\Controller;

use App\Entity\AfriCard;
use App\Form\AfriCardType;
use App\Repository\AfriCardRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/africard")
 */
class AfriCardController extends AbstractController
{
    /**
     * @Route("/", name="afri_card_index", methods={"GET"})
     */
    public function index(AfriCardRepository $afriCardRepository): Response
    {
        return $this->render('afri_card/index.html.twig', [
            'afri_cards' => $afriCardRepository->findAll(),
        ]);
        
    }
    

    /**
     * @Route("/new", name="afri_card_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $afriCard = new AfriCard();
        $form = $this->createForm(AfriCardType::class, $afriCard);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $afriCard->setDateSave(new \DateTime());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($afriCard);
            $entityManager->flush();

            return $this->redirectToRoute('afri_card_index');
        }

        return $this->render('afri_card/new.html.twig', [
            'afri_card' => $afriCard,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="afri_card_show", methods={"GET"})
     */
    public function show(AfriCard $afriCard): Response
    {
        return $this->render('afri_card/show.html.twig', [
            'afri_card' => $afriCard,
        ]); 
    }

    /**
     * @Route("/{id}/edit", name="afri_card_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, AfriCard $afriCard): Response
    {
        $form = $this->createForm(AfriCardType::class, $afriCard);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('afri_card_index');
        }

        return $this->render('afri_card/edit.html.twig', [
            'afri_card' => $afriCard,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="afri_card_delete", methods={"DELETE"})
     */
    public function delete(Request $request, AfriCard $afriCard): Response
    {
        if ($this->isCsrfTokenValid('delete'.$afriCard->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($afriCard);
            $entityManager->flush();
        }

        return $this->redirectToRoute('afri_card_index');
    }

   
}
