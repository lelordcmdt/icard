<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Repository\OperationRepository;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends AbstractController
{
    /**
     * @Route("/", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {       //OperationRepository $operationRepository
            $session = new Session();
           /*$agency = $this->getUser()->getAgency();
            $coffer = (int)$agency->getBalance();
            $list = $operationRepository->findByDateAndUser($this->getUser(), ($session->get('currentDate'))->format('Y-m-d'));
            
            $debit = 0;
            $credit = 0;     
            foreach ($list as $item) {
                //echo "<script>alert('".$item->getAmount()."');</script>";
                if($item->getOccasion() != null && $item->getOccasion()->getTypeOperation() == \App\Helper\Utils::$OP_DEBIT){
                    //$item->getAmount()
                     $debit =  $debit + $item->getAmount();
                } else {
                     $credit =  $credit + $item->getAmount();
                }
            }
            
            $balance = $coffer + $credit - $debit;
        */
            //$session->remove('currentDate');
            $session->remove('cards');
            throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
    
    /**
     * @Route("/close", name="close")
     */
    public function closeDay()
    {   //OperationRepository $operationRepository
        $session = new Session();
        $session->remove('currentDate');
        //$session->remove('cards');
        $session->set('currentDate', new \DateTime());
        return $this->redirectToRoute('operation_index');
    }
}
