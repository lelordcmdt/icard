<?php

namespace App\Controller;

use App\Entity\Occasion;
use App\Form\OccasionType;
use App\Repository\OccasionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/occasion")
 */
class OccasionController extends AbstractController
{
    /**
     * @Route("/", name="occasion_index", methods={"GET"})
     */
    public function index(OccasionRepository $occasionRepository): Response
    {
        return $this->render('occasion/index.html.twig', [
            'occasions' => $occasionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="occasion_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $occasion = new Occasion();
        $form = $this->createForm(OccasionType::class, $occasion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($occasion);
            $entityManager->flush();

            return $this->redirectToRoute('occasion_index');
        }

        return $this->render('occasion/new.html.twig', [
            'occasion' => $occasion,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="occasion_show", methods={"GET"})
     */
    public function show(Occasion $occasion): Response
    {
        return $this->render('occasion/show.html.twig', [
            'occasion' => $occasion,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="occasion_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Occasion $occasion): Response
    {
        $form = $this->createForm(OccasionType::class, $occasion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('occasion_index');
        }

        return $this->render('occasion/edit.html.twig', [
            'occasion' => $occasion,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="occasion_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Occasion $occasion): Response
    {
        if ($this->isCsrfTokenValid('delete'.$occasion->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($occasion);
            $entityManager->flush();
        }

        return $this->redirectToRoute('occasion_index');
    }
}
