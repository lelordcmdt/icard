<?php

namespace App\Controller;

use App\Entity\CofferState;
use App\Form\CofferStateType;
use App\Repository\CofferStateRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\OperationRepository;
use App\Model\SearchDTO;
use App\Form\SearchType;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Helper\Utils;


/**
 * @Route("/coffer-state")
 */
class CofferStateController extends AbstractController
{
    
   private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }
    
    
     /**
     * @Route("/list", name="coffer_state_list", methods={"GET", "POST"})
     */
    public function list(CofferStateRepository $cofferStateRepository, Request $request): Response
    {
        $model = new SearchDTO();
        $form = $this->createForm(SearchType::class, $model, ['index' => 0]);
        $form->handleRequest($request);
        
        $coffer_states = $cofferStateRepository->findAll();
        
        if ($form->isSubmitted() && $form->isValid()) {
            
          
            if($model->getAgency() != null) 
                $coffer_states = $cofferStateRepository->findBetweenTwoDateAndUser($model->getAgency()->getUsers()[0], $model->getStartDate(), $model->getEndDate());
            else 
                $coffer_states = $cofferStateRepository->findBetweenTwoDate($model->getStartDate(), $model->getEndDate());

        }
        
        //$coffer_states = $cofferStateRepository->findAll();
        //echo ("<script>alert(".count($coffer_states).");</script>");
        
        return $this->render('coffer_state/index.html.twig', [
            'coffer_states' => $coffer_states,
            'balance' => 0,
            'form' => $form->createView(),
        ]);
    }

    
    /**
     * @Route("/{balance}/list", name="coffer_state_index", methods={"GET"})
     */
    public function index($balance, CofferStateRepository $cofferStateRepository, OperationRepository $operationRepository, Request $request): Response
    {
       
        $this->session->set("ecart",
            (int)$balance
        );
      
        if($this->isGranted("ROLE_USER") && !$this->isGranted("ROLE_ADMIN")) {
            
            $coffer = $operationRepository->countByCredit(Utils::$OP_DEBIT, ($this->session->get('currentDate'))->format('Y-m-d H:i:s'))[0]["totalcredit"] - $operationRepository->countByDebit(Utils::$OP_DEBIT, ($this->session->get('currentDate'))->format('Y-m-d H:i:s'))[0]["totaldebit"];
        
            if($this->isGranted("ROLE_USER") && !$this->isGranted("ROLE_ADMIN")) {
                $coffer = $operationRepository->countByTypeOperation($this->getUser(), Utils::$OP_DEBIT, ($this->session->get('currentDate'))->format('Y-m-d H:i:s'))[0]["credit"] - $operationRepository->countByTypeOperation($this->getUser(), Utils::$OP_DEBIT, ($this->session->get('currentDate'))->format('Y-m-d H:i:s'))[0]["credit"];
                $list = $operationRepository->findByDateAndUser($this->getUser(), ($this->session->get('currentDate'))->format('Y-m-d H:i:s'));
            }

            $list = $operationRepository->findByDateAndUser($this->getUser(), ($this->session->get('currentDate'))->format('Y-m-d H:i:s'));
           
            $coffer_states = $cofferStateRepository->findByDateAndUser($this->getUser(), $this->session->get('currentDate')->format('Y-m-d'));
       
        } else {
           $coffer_states = $cofferStateRepository->findAll();
        }
        
        //$coffer_states = $cofferStateRepository->findAll();
        //echo ("<script>alert(".count($coffer_states).");</script>");
        
        return $this->render('coffer_state/index.html.twig', [
            'coffer_states' => $coffer_states,
            'balance' => (int)$this->session->get('ecart'),
            //'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="coffer_state_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
       // var_dump($request);
        $cofferState = new CofferState();
        $form = $this->createForm(CofferStateType::class, $cofferState);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $cofferState->setUser($this->getUser());
            $cofferState->setDateState($this->session->get('currentDate'));
            
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($cofferState);
            $entityManager->flush();


            return $this->redirectToRoute('coffer_state_index', ["balance" => (int)$this->session->get('ecart')]);
        }

       
        return $this->render('coffer_state/new.html.twig', [
            'coffer_state' => $cofferState,
            'form' => $form->createView(),
            'balance' => (int)$this->session->get('ecart'),
        ]);
    }

    /**
     * @Route("/{id}", name="coffer_state_show", methods={"GET"})
     */
    public function show(CofferState $cofferState): Response
    {
        return $this->render('coffer_state/show.html.twig', [
            'coffer_state' => $cofferState,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="coffer_state_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, CofferState $cofferState): Response
    {
        $form = $this->createForm(CofferStateType::class, $cofferState);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $cofferState->setUser($this->getUser());
            $cofferState->setDateState($this->session->get('currentDate'));
            
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('coffer_state_index');
        }

        return $this->render('coffer_state/edit.html.twig', [
            'coffer_state' => $cofferState,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="coffer_state_delete", methods={"DELETE"})
     */
    public function delete(Request $request, CofferState $cofferState): Response
    {
        if ($this->isCsrfTokenValid('delete'.$cofferState->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($cofferState);
            $entityManager->flush();
        }

        return $this->redirectToRoute('coffer_state_index');
    }
}
