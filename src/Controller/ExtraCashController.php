<?php

namespace App\Controller;

use App\Entity\ExtraCash;
use App\Form\ExtraCashType;
use App\Repository\ExtraCashRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/extra/cash")
 */
class ExtraCashController extends AbstractController
{
    /**
     * @Route("/", name="extra_cash_index", methods={"GET"})
     */
    public function index(ExtraCashRepository $extraCashRepository): Response
    {
        return $this->render('extra_cash/index.html.twig', [
            'extra_cashes' => $extraCashRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="extra_cash_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $extraCash = new ExtraCash();
        $form = $this->createForm(ExtraCashType::class, $extraCash);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($extraCash);
            $entityManager->flush();

            return $this->redirectToRoute('extra_cash_index');
        }

        return $this->render('extra_cash/new.html.twig', [
            'extra_cash' => $extraCash,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="extra_cash_show", methods={"GET"})
     */
    public function show(ExtraCash $extraCash): Response
    {
        return $this->render('extra_cash/show.html.twig', [
            'extra_cash' => $extraCash,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="extra_cash_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ExtraCash $extraCash): Response
    {
        $form = $this->createForm(ExtraCashType::class, $extraCash);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('extra_cash_index');
        }

        return $this->render('extra_cash/edit.html.twig', [
            'extra_cash' => $extraCash,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="extra_cash_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ExtraCash $extraCash): Response
    {
        if ($this->isCsrfTokenValid('delete'.$extraCash->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($extraCash);
            $entityManager->flush();
        }

        return $this->redirectToRoute('extra_cash_index');
    }
    
}
