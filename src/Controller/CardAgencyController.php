<?php

namespace App\Controller;

use App\Entity\CardAgency;
use App\Form\CardAgencyType;
use App\Repository\CardAgencyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Repository\UserRepository;

/**
 * @Route("/card/agency")
 */
class CardAgencyController extends AbstractController
{
    
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }
    
    /**
     * @Route("/", name="card_agency_index", methods={"GET"})
     */
    public function index(CardAgencyRepository $cardAgencyRepository): Response
    {
        return $this->render('card_agency/index.html.twig', [
            'card_agencies' => $cardAgencyRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="card_agency_new", methods={"GET","POST"})
     */
    public function new(Request $request, UserRepository $userRepository): Response
    {
        //, CardAgencyRepository $cardAgencyRepository
        $cardAgency = new CardAgency();
        $form = $this->createForm(CardAgencyType::class, $cardAgency);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $entityManager = $this->getDoctrine()->getManager();
            $cardAgency->setDateAdd($this->session->get('currentDate'));
            $cardAgency->setUser($this->getUser());
            
            $qty = $cardAgency->getQuantity();
            $user = $userRepository->find($this->getUser()->getId());
            
               
            if(!$this->isGranted("ROLE_ADMIN")) {
              
                $card = $user->getAgency()->getAgencyCards()[0];
                
                if($card->getQuantity() > $qty){
                    
                    $cardAgency = $cardAgency->getAgency()->getAgencyCards()[0];
                    $cardAgency->setQuantity($cardAgency->getQuantity() + $qty);
                    $card->setQuantity($card->getQuantity() - $qty);
                    $entityManager->persist($cardAgency);
                    $entityManager->persist($card);
                    $entityManager->flush();
                    //$entityManager->flush();
                }
                        
            } else {
               // $card = $user->getAgencyCards()[0];

               // if($card->getQuantity() > $qty){
                    $cardAgency = $cardAgency->getAgency()->getAgencyCards()[0];
                    $cardAgency->setQuantity($cardAgency->getQuantity() + $qty);
                    $entityManager->persist($cardAgency);
                    $entityManager->flush();
               // }
                
            }
            
            return $this->redirectToRoute('card_agency_index');
            
        }

        return $this->render('card_agency/new.html.twig', [
            'card_agency' => $cardAgency,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="card_agency_show", methods={"GET"})
     */
    public function show(CardAgency $cardAgency): Response
    {
        return $this->render('card_agency/show.html.twig', [
            'card_agency' => $cardAgency,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="card_agency_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, CardAgency $cardAgency): Response
    {
        $form = $this->createForm(CardAgencyType::class, $cardAgency);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //$cardAgency->setDateAdd($this->session->get('currentDate'));
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('card_agency_index');
        }

        return $this->render('card_agency/edit.html.twig', [
            'card_agency' => $cardAgency,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="card_agency_delete", methods={"DELETE"})
     */
    public function delete(Request $request, CardAgency $cardAgency): Response
    {
        if ($this->isCsrfTokenValid('delete'.$cardAgency->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($cardAgency);
            $entityManager->flush();
        }

        return $this->redirectToRoute('card_agency_index');
    }
}
