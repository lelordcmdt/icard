<?php

namespace App\Controller;

use App\Entity\Operation;
use App\Form\OperationType;
use App\Repository\OperationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Helper\Utils;
use App\Model\SearchDTO;
use App\Form\SearchType;
use App\Model\CardSaleDTO;
use App\Form\CardSaleType;
use App\Repository\AfriCardRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Repository\CardAgencyRepository;
use App\Repository\OccasionRepository;

/**
 * @Route("/operation")
 */
class OperationController extends AbstractController
{
    
    private $session;

    public function __construct(SessionInterface $session) {
        $this->session = $session;
    }
    
    /**
     * @Route("/", name="operation_index", methods={"GET","POST"})
     */
    public function index(OperationRepository $operationRepository, Request $request): Response
    {
        $model = new SearchDTO();
        $form = $this->createForm(SearchType::class, $model, ['index' => 0]);
        $form->handleRequest($request);
        $list = $operationRepository->findAll();
        //$list = $operationRepository->findByDate(($this->session->get('currentDate'))->format('Y-m-d H:i:s'));  
        /*if($this->isGranted("ROLE_ADMIN")) {
            $list = $operationRepository->findByDate(new \DateTime());  
           // $list = $operationRepository->findByDate(new \DateTime());  
        }*/
        
        //var_dump($operationRepository->countByUserAndCredit($this->getUser(), Utils::$OP_DEBIT)[0]["credit"]);die;
        $coffer = $operationRepository->countByCredit(Utils::$OP_DEBIT, ($this->session->get('currentDate'))->format('Y-m-d H:i:s'))[0]["totalcredit"] - $operationRepository->countByDebit(Utils::$OP_DEBIT, ($this->session->get('currentDate'))->format('Y-m-d H:i:s'))[0]["totaldebit"];
        if($this->isGranted("ROLE_USER") && !$this->isGranted("ROLE_ADMIN")) {
            $coffer = $operationRepository->countByTypeOperation($this->getUser(), Utils::$OP_DEBIT, ($this->session->get('currentDate'))->format('Y-m-d H:i:s'))[0]["credit"] - $operationRepository->countByUserAndDebit($this->getUser(), Utils::$OP_DEBIT, ($this->session->get('currentDate'))->format('Y-m-d H:i:s'))[0]["debit"];
            $list = $operationRepository->findByDateAndUser($this->getUser(), ($this->session->get('currentDate'))->format('Y-m-d H:i:s'));
        }
       
        if ($form->isSubmitted() && $form->isValid()) {
            //echo '<script>alert('.(string)$model->getStartDate().');</script>';
            //var_dump($model->getStartDate());
            if($this->isGranted("ROLE_ADMIN")) {
                if($model->getAgency() != null) {
                    if($model->getOccasion() != null) {
                        //1
                        $list = $operationRepository->findBetweenDateAndOccasionAndUser($model->getAgency()->getUsers()[0], $model->getOccasion(), $model->getStartDate(), $model->getEndDate());
       
                    } else {
                        //2
                        $list = $operationRepository->findBetweenDateAndUser($model->getAgency()->getUsers()[0], $model->getStartDate(), $model->getEndDate());
       
                    }
                } else {
                    if($model->getOccasion() != null) {
                        //3
                        $list = $operationRepository->findBetweenDateAndOccasion($model->getOccasion(), $model->getStartDate(), $model->getEndDate());
       
                    } else {
                        //4
                        $list = $operationRepository->findBetweenDate($model->getStartDate(), $model->getEndDate());
       
                    }
                }
            } else {
                if($model->getOccasion() != null) {
                    //1
                    $list = $operationRepository->findBetweenDateAndOccasion($model->getOccasion(), $model->getStartDate(), $model->getEndDate());
       
                } else {
                    //2
                    $list = $operationRepository->findBetweenDate($model->getStartDate(), $model->getEndDate());
       
                }
            }
        }
        
        return $this->render('operation/index.html.twig', [
            'operations' => $list,
            'type' => Utils::$OP_DEBIT,
            'form' => $form->createView(),
            'coffer' => $coffer,
        ]);
        
    }

     
    
    
    /**
     * @Route("/new", name="operation_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $operation = new Operation();
        $form = $this->createForm(OperationType::class, $operation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $operation->setUser($this->getUser());
            $operation->setDateOperation($this->session->get('currentDate'));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($operation);
            $entityManager->flush();

            return $this->redirectToRoute('operation_index');
        }

        return $this->render('operation/new.html.twig', [
            'operation' => $operation,
            'form' => $form->createView(),
        ]);
        
    }

      
       
    /**
     * @Route("/manager/sale", name="operation_sale", methods={"GET","POST"})
     */
    public function sale(Request $request, CardAgencyRepository $cardAgencyRepository, OccasionRepository $occasionRepository): Response
    {
        
        $model = new CardSaleDTO();
        $form = $this->createForm(CardSaleType::class, $model);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            
            //echo "<script>alert('hello');</script>";
            if($model->getCardType()->getTypeCard() == Utils::$CARD_UBA_BF || $model->getCardType()->getTypeCard() == Utils::$CARD_UBA_CI) {
                $card = $cardAgencyRepository->findBy(["card" => $model->getCardType(), "agency" => $this->getUser()->getAgency()])[0];
               // var_dump($card->getCard()->getTypeCard());
                $diff = $card->getQuantity() - $model->getQuantity();
                if ($diff >= 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $card->setQuantity($diff);
                    
                    $operation = new Operation();
                    $operation->setUser($this->getUser());
                    $operation->setDateOperation($this->session->get('currentDate'));
                    $operation->setAmount($card->getCard()->getPrice() * $model->getQuantity());
                    $operation->setComment(Utils::$OP_CREDIT);
                    $occasion = $occasionRepository->findBy(['name' => 'Vente de carte '.$model->getCardType()->getTypeCard()])[0];
                    $operation->setOccasion($occasion);
                    $operation->setWording($model->getClientName());
                    
                    $entityManager->persist($card);
                    $entityManager->persist($operation);
                    
                    $entityManager->flush();
                    
                    return $this->redirectToRoute('operation_index');
                }
                
            }
           
        }

        return $this->render('operation/card_sale.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    
    /**
     * @Route("/{id}", name="operation_show", methods={"GET"})
     */
    public function show(Operation $operation): Response
    {
        return $this->render('operation/show.html.twig', [
            'operation' => $operation,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="operation_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Operation $operation): Response
    {
        $form = $this->createForm(OperationType::class, $operation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $operation->setUser($this->getUser());
            $operation->setDateOperation($this->session->get('currentDate'));
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('operation_index');
        }

        return $this->render('operation/edit.html.twig', [
            'operation' => $operation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="operation_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Operation $operation, CardAgencyRepository $cardAgencyRepository, AfriCardRepository $afriCardRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$operation->getId(), $request->request->get('_token'))) {
            
            $entityManager = $this->getDoctrine()->getManager();
            
            if($operation->getOccasion()->getName() == Utils::$SERVICES[6] || $operation->getOccasion()->getName() == Utils::$SERVICES[7]) {
                
                //$list = explode("-", $operation->getWording());trim($list[1])
                $typeCard = $afriCardRepository->findOneBy(["typeCard" => Utils::$CARD_UBA_BF]);
                if (strpos($operation->getOccasion()->getName(), Utils::$CARD_UBA_CI) !== false) {
                    $typeCard = $afriCardRepository->findOneBy(["typeCard" => Utils::$CARD_UBA_CI]);
                }
                
                $card = $cardAgencyRepository->findBy(["card" => $typeCard, "agency" => $this->getUser()->getAgency()])[0];
                $card->setQuantity($card->getQuantity() + ($operation->getAmount() / $card->getCard()->getPrice()));
                
                $entityManager->persist($card);
                $entityManager->persist($operation);
                $entityManager->flush(); 
                
            }
            
            $entityManager->remove($operation);
            $entityManager->flush();
        }

        return $this->redirectToRoute('operation_index');
    }
   
}
