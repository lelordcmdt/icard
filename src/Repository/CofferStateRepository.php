<?php

namespace App\Repository;

use App\Entity\CofferState;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CofferState|null find($id, $lockMode = null, $lockVersion = null)
 * @method CofferState|null findOneBy(array $criteria, array $orderBy = null)
 * @method CofferState[]    findAll()
 * @method CofferState[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CofferStateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CofferState::class);
    }

    
    
    
    public function findBetweenTwoDate($startDate, $endDate)
    {
        $sDate = $startDate;
        if(is_array($startDate)){
           $sDate = new \DateTime($startDate["year"]."-".$startDate["month"]."-".$startDate["day"]);
        }
        
        $eDate = $endDate;
        if(is_array($endDate)){
           $eDate = new \DateTime($endDate["year"]."-".$endDate["month"]."-".$endDate["day"]);
        }
        
        $qb = $this->createQueryBuilder('c')
                   ->andWhere('c.dateState >= :startDate')
                   ->andWhere('c.dateState <= :endDate')
                   ->orderBy('c.id', 'DESC')
                   ->setParameters(['startDate' => $sDate, 'endDate' => $eDate]);
        
        return $qb->getQuery()->getResult();
    }
    
    public function findBetweenTwoDateAndUser($user, $startDate, $endDate)
    {
        $sDate = $startDate;
        if(is_array($startDate)){
           $sDate = new \DateTime($startDate["year"]."-".$startDate["month"]."-".$startDate["day"]);
        }
        
        $eDate = $endDate;
        if(is_array($endDate)){
           $eDate = new \DateTime($endDate["year"]."-".$endDate["month"]."-".$endDate["day"]);
        }
        
        $qb = $this->createQueryBuilder('c')
                   ->andWhere('c.user = :user')
                   ->andWhere('c.dateState >= :startDate')
                   ->andWhere('c.dateState <= :endDate')
                   ->orderBy('c.id', 'DESC')
                   ->setParameters(['startDate' => $sDate, 'endDate' => $eDate, 'user' => $user]);
        
        return $qb->getQuery()->getResult();
        
    }
    
   
    public function findByDateAndUser($user, $sameDate){
          
        $qb = $this->createQueryBuilder('c')
                   ->andWhere('c.user = :user')
                   ->andWhere('c.dateState >= :startDate')
                   ->orderBy('c.id', 'DESC')
                   ->setParameters(['startDate' => $sameDate, 'user' => $user]);
        
        return $qb->getQuery()->getResult();
       
    }
    
    
    // /**
    //  * @return CofferState[] Returns an array of CofferState objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CofferState
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
