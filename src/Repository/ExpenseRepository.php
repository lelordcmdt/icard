<?php

namespace App\Repository;

use App\Entity\Expense;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Expense|null find($id, $lockMode = null, $lockVersion = null)
 * @method Expense|null findOneBy(array $criteria, array $orderBy = null)
 * @method Expense[]    findAll()
 * @method Expense[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExpenseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Expense::class);
    }

      
    public function findByBetweenTwoDate($startDate, $endDate)
    {
        $sDate = $startDate;
        if(isset($startDate["year"])){
           $sDate = new \DateTime($startDate["year"]."-".$startDate["month"]."-".$startDate["day"]);
        }
        
        $eDate = $endDate;
        if(isset($endDate["year"])){
           $eDate = new \DateTime($endDate["year"]."-".$endDate["month"]."-".$endDate["day"]);
        }
        
        $qb = $this->createQueryBuilder('e')
                   ->andWhere('e.dateExp >= :startDate')
                   ->andWhere('e.dateExp <= :endDate')
                   ->orderBy('e.id', 'DESC')
                   ->setParameters(['startDate' => $sDate, 'endDate' => $eDate]);
        
        return $qb->getQuery()->getResult();
    }
    
    public function findByBetweenTwoDateAndUser($user, $startDate, $endDate)
    {
        $sDate = $startDate;
        if(isset($startDate["year"])){
           $sDate = new \DateTime($startDate["year"]."-".$startDate["month"]."-".$startDate["day"]);
        }
        
        $eDate = $endDate;
        if(isset($endDate["year"])){
           $eDate = new \DateTime($endDate["year"]."-".$endDate["month"]."-".$endDate["day"]);
        }
        
        $qb = $this->createQueryBuilder('e')
                   ->andWhere('e.user = :user')
                   ->andWhere('e.dateExp >= :startDate')
                   ->andWhere('e.dateExp <= :endDate')
                   ->orderBy('e.id', 'DESC')
                   ->setParameters(['startDate' => $sDate, 'endDate' => $eDate, 'user' => $user]);
        
        return $qb->getQuery()->getResult();
        
    }
    
    
    // /**
    //  * @return Expense[] Returns an array of Expense objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Expense
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
