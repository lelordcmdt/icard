<?php

namespace App\Repository;

use App\Entity\CardAgency;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CardAgency|null find($id, $lockMode = null, $lockVersion = null)
 * @method CardAgency|null findOneBy(array $criteria, array $orderBy = null)
 * @method CardAgency[]    findAll()
 * @method CardAgency[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CardAgencyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CardAgency::class);
    }
    
    // /**
    //  * @return CardAgency[] Returns an array of CardAgency objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CardAgency
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
