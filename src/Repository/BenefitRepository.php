<?php

namespace App\Repository;

use App\Entity\Benefit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Benefit|null find($id, $lockMode = null, $lockVersion = null)
 * @method Benefit|null findOneBy(array $criteria, array $orderBy = null)
 * @method Benefit[]    findAll()
 * @method Benefit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BenefitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Benefit::class);
    }

     
    
    public function findByDate($sameDate)
    {
        $sDate = $sameDate;
        $eDate = $sameDate;
        if(is_array($sameDate)){
            $sDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]." 00:00:00");
            $eDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]." 23:59:59");
        }
        
        $qb = $this->createQueryBuilder('b')
                   ->andWhere('b.dateBen >= :from')
                   ->andWhere('b.dateBen <= :to')
                   ->orderBy('b.id', 'DESC')
                   ->setParameters(['from' => $sDate, 'to' => $eDate]);
        
        return $qb->getQuery()->getResult();
    }
    
    public function findByRole($role)
    {
        $qb = $this->createQueryBuilder('b')
                   ->join('b.user', 'u')
                   ->andWhere('u.roles LIKE :role')
                   ->orderBy('b.id', 'DESC')
                   ->setParameter('role', '%'.$role.'%');
        
        return $qb->getQuery()->getResult();
    }
    
    public function findBetweenTwoDate($startDate, $endDate, $role)
    {
        // 
        $sDate = $startDate;
        if(is_array($startDate)){
           $sDate = new \DateTime($startDate["year"]."-".$startDate["month"]."-".$startDate["day"]." 00:00:00");
        }
        
        $eDate = $endDate;
        if(is_array($endDate)){
           $eDate = new \DateTime($endDate["year"]."-".$endDate["month"]."-".$endDate["day"]." 23:59:59");
        }
        
        $qb = $this->createQueryBuilder('b')
                   ->join('b.user', 'u')
                   ->andWhere('u.roles LIKE :role')
                   ->andWhere('b.dateBen >= :startDate')
                   ->andWhere('b.dateBen <= :endDate')
                   ->orderBy('b.id', 'DESC')
                   ->setParameters(['role' => '%'.$role.'%', 'startDate' => $sDate, 'endDate' => $eDate]);
        
        return $qb->getQuery()->getResult();
    }
    
    public function findBetweenTwoDateAndUser($user, $startDate, $endDate, $role)
    {
        $sDate = $startDate;
        if(is_array($startDate)){
           $sDate = new \DateTime($startDate["year"]."-".$startDate["month"]."-".$startDate["day"]);
        }
        
        $eDate = $endDate;
        if(is_array($endDate)){
           $eDate = new \DateTime($endDate["year"]."-".$endDate["month"]."-".$endDate["day"]);
        }
        
        $qb = $this->createQueryBuilder('b')
                   ->join('b.user', 'u')
                   ->andWhere('u = :user')
                   ->andWhere('u.roles LIKE :role')
                   ->andWhere('b.dateBen >= :startDate')
                   ->andWhere('b.dateBen <= :endDate')
                   ->orderBy('b.id', 'DESC')
                   ->setParameters(['role' => '%'.$role.'%', 'startDate' => $sDate, 'endDate' => $eDate, 'user' => $user]);
        
        return $qb->getQuery()->getResult();
        
    }
    
    //Liste Par date de connexion
    public function findByDateAndUser($user, $sameDate){
          
        $qb = $this->createQueryBuilder('b')
                   ->andWhere('b.user = :user')
                   ->andWhere('b.dateBen >= :startDate')
                   ->orderBy('b.id', 'DESC')
                   ->setParameters(['startDate' => $sameDate, 'user' => $user]);
        
        return $qb->getQuery()->getResult();
       
    }
    
  
    
    // /**
    //  * @return Benefit[] Returns an array of Benefit objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Benefit
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
