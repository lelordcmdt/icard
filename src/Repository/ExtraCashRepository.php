<?php

namespace App\Repository;

use App\Entity\ExtraCash;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ExtraCash|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExtraCash|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExtraCash[]    findAll()
 * @method ExtraCash[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExtraCashRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExtraCash::class);
    }

    // /**
    //  * @return ExtraCash[] Returns an array of ExtraCash objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ExtraCash
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
