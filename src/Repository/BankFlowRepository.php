<?php

namespace App\Repository;

use App\Entity\BankFlow;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BankFlow|null find($id, $lockMode = null, $lockVersion = null)
 * @method BankFlow|null findOneBy(array $criteria, array $orderBy = null)
 * @method BankFlow[]    findAll()
 * @method BankFlow[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BankFlowRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BankFlow::class);
    }

    public function findByBetweenTwoDate($startDate, $endDate)
    {
        $sDate = $startDate;
        if(isset($startDate["year"])){
           $sDate = new \DateTime($startDate["year"]."-".$startDate["month"]."-".$startDate["day"]);
        }
        
        $eDate = $endDate;
        if(isset($endDate["year"])){
           $eDate = new \DateTime($endDate["year"]."-".$endDate["month"]."-".$endDate["day"]);
        }
        
        $qb = $this->createQueryBuilder('b')
                   ->andWhere('b.flowDate >= :startDate')
                   ->andWhere('b.flowDate <= :endDate')
                   ->orderBy('b.id', 'DESC')
                   ->setParameters(['startDate' => $sDate, 'endDate' => $eDate]);
        
        return $qb->getQuery()->getResult();
    }
    
    
    // /**
    //  * @return BankFlow[] Returns an array of BankFlow objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BankFlow
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
