<?php

namespace App\Repository;

use App\Entity\Operation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Operation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Operation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Operation[]    findAll()
 * @method Operation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */

class OperationRepository extends ServiceEntityRepository
{//date('Y-m-01'); fist day of the month 
    
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Operation::class);
    }
    
    public function countByOccasion($occasion, $type)
    {
        $qb = $this->createQueryBuilder('o')
                   ->join('o.occasion', 'oc')
                   ->andWhere('oc.name = :val')
                   ->andWhere('oc.typeOperation = :type')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['val' => $occasion, 'type' => $type])
                   ->select('SUM(o.amount) as total');
        
        return $qb->getQuery()->getResult();
    }
    
    public function countByOccasionAndUser($user, $occasion, $type)
    {
        $qb = $this->createQueryBuilder('o')
                   ->join('o.occasion', 'oc')
                   ->andWhere('oc.name = :val')
                   ->andWhere('oc.typeOperation = :type')
                   ->andWhere('o.user = :user')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['val' => $occasion, 'user' => $user, 'type' => $type])
                   ->select('SUM(o.amount) as total');
        
        return $qb->getQuery()->getResult();
    }
    
       
    public function countByDateAndOccasion($sameDate, $occasion)
    {
        $sDate = $sameDate;
        if(is_array($sameDate)){
              $sDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]);
        }
        
      
        $qb = $this->createQueryBuilder('o')
                   ->andWhere('o.dateOperation <= :from')
                   ->andWhere('o.occasion = :occasion')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['from' => $sDate, 'occasion' => $occasion])
                   ->select('SUM(o.amount) as totalcredit');
        
        return $qb->getQuery()->getResult();
    }
    
    
    public function countByDateAndOccasionAndUser($sameDate, $occasion, $user)
    {
        $sDate = $sameDate;
        if(is_array($sameDate)){
            $sDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]);
        }
        
        $qb = $this->createQueryBuilder('o')
                   ->andWhere('o.dateOperation <= :from')
                   ->andWhere('o.occasion = :occasion')
                   ->andWhere('o.user = :user')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['from' => $sDate, 'occasion' => $occasion, 'user' => $user])
                   ->select('SUM(o.amount) as totalcredit');
        
        return $qb->getQuery()->getResult();
    }
    
    
    public function findByUserAndOccasion($user, $occasion)
    {
        //$sDate = new \DateTime('first day of this month');
        $qb = $this->createQueryBuilder('o')
                  // ->andWhere('o.dateOperation < :from')
                   ->andWhere('o.occasion = :occasion')
                   //->andWhere('o.user = :user')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['occasion' => $occasion]);
        
        
        if($user != null) {
            $qb = $this->createQueryBuilder('o')
                  // ->andWhere('o.dateOperation < :from')
                   ->andWhere('o.occasion = :occasion')
                   ->andWhere('o.user = :user')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['user' => $user, 'occasion' => $occasion]);
        
        }
        
        
        
        return $qb->getQuery()->getResult();
    }
    
    public function findByOccasion($occasion)
    {
        $sDate = new \DateTime('first day of this month');
        
        $qb = $this->createQueryBuilder('o')
                   ->andWhere('o.dateOperation < :from')
                   ->andWhere('o.occasion = :occasion')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['from' => $sDate, 'occasion' => $occasion]);
        
        return $qb->getQuery()->getResult();
    }
    
    public function countByCard($occasion)
    {
        $sDate = new \DateTime('first day of this month');
        //if(is_array($sameDate)){
          //$sDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]);
        //}
        
        $qb = $this->createQueryBuilder('o')
                   ->andWhere('o.occasion = :type')
                   ->andWhere('o.dateOperation <= :from')
                 //->andWhere('o.dateOperation >= :from')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['from' => $sDate, 'type' => $occasion])
                   ->select('SUM(o.amount) as totalcredit');
        
        return $qb->getQuery()->getResult();
    }
    
    public function countByCards($occasion)
    {
      
        $qb = $this->createQueryBuilder('o')
                   ->andWhere('o.occasion = :type')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['type' => $occasion])
                   ->select('SUM(o.amount) as totalcredit');
        
        return $qb->getQuery()->getResult();
    }
    
    public function countByUserCard($user, $occasion)
    {
        $sDate = new \DateTime('first day of this month');;
        //if(is_array($sameDate)){
          //$sDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]);
        //}
        
        $qb = $this->createQueryBuilder('o')
                   ->andWhere('o.occasion = :type')
                   ->andWhere('o.dateOperation < :from')
                   ->andWhere('o.user = :user')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['user' => $user, 'from' => $sDate, 'type' => $occasion])
                   ->select('SUM(o.amount) as totalcredit');
        
        return $qb->getQuery()->getResult();
    }
    
    public function findCards($occasion)
    {
       
        $qb = $this->createQueryBuilder('o')
                   ->andWhere('o.occasion = :type')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['type' => $occasion]);
                  
        return $qb->getQuery()->getResult();
    }
    
    
    
    
    public function countByTypeOperation($user, $type, $sameDate)
    {
        $sDate = $sameDate;
        if(is_array($sameDate)){
          $sDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]);
        }
        
        $qb = $this->createQueryBuilder('o')
                   ->join('o.occasion', 'oc')
                   ->andWhere('oc.typeOperation != :type')
                   //->andWhere('o.dateOperation < :from')
                   ->andWhere('o.user = :user')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters([/*'from' => $sDate, */'user' => $user, 'type' => $type])
                   ->select('SUM(o.amount) as credit');
        
        return $qb->getQuery()->getResult();
    }
    
    public function countByUserAndDebit($user, $type, $sameDate)
    {
        $sDate = $sameDate;
        if(is_array($sameDate)){
          $sDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]);
        }
        
        $qb = $this->createQueryBuilder('o')
                   ->join('o.occasion', 'oc')
                   ->andWhere('oc.typeOperation = :type')
                   //->andWhere('o.dateOperation < :from')
                   ->andWhere('o.user = :user')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters([/*'from' => $sDate, */ 'user' => $user, 'type' => $type])
                   ->select('SUM(o.amount) as debit');
        
        return $qb->getQuery()->getResult();
    }
    
    public function countByDebit($type, $sameDate)
    {
          $sDate = $sameDate;
        if(is_array($sameDate)){
          $sDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]);
        }
        
        $qb = $this->createQueryBuilder('o')
                   ->join('o.occasion', 'oc')
                   ->andWhere('oc.typeOperation = :type')
                   //->andWhere('o.dateOperation < :from')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters([/*'from' => $sDate, */'type' => $type])
                   ->select('SUM(o.amount) as totaldebit');
        
        return $qb->getQuery()->getResult();
    }
    
   
    
    public function countByCredit($type, $sameDate)
    {
        $sDate = $sameDate;
        if(is_array($sameDate)){
          $sDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]);
        }
        
        $qb = $this->createQueryBuilder('o')
                   ->join('o.occasion', 'oc')
                   ->andWhere('oc.typeOperation != :type')
                   //->andWhere('o.dateOperation <= :from')
                   //->andWhere('o.dateOperation >= :from')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters([/*'from' => $sDate, */'type' => $type])
                   ->select('SUM(o.amount) as totalcredit');
        
        return $qb->getQuery()->getResult();
    }
    
    
    public function countByDebitAndOccasion($occasion, $type, $sameDate)
    {
        $sDate = $sameDate;
        if(is_array($sameDate)){
          $sDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]);
        }
        
        $qb = $this->createQueryBuilder('o')
                   ->join('o.occasion', 'oc')
                    ->andWhere('oc = :occasion')
                   ->andWhere('oc.typeOperation = :type')
                   ->andWhere('o.dateOperation < :from')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['from' => $sDate, 'type' => $type, 'occasion' => $occasion])
                   ->select('SUM(o.amount) as totaldebit');
        
        return $qb->getQuery()->getResult();
    }
    
   
    public function countByCreditAndOccasion($occasion, $type, $sameDate)
    {
        $sDate = $sameDate;
        if(is_array($sameDate)){
          $sDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]);
        }
        
        $qb = $this->createQueryBuilder('o')
                   ->join('o.occasion', 'oc')
                   ->andWhere('oc = :occasion')
                   ->andWhere('oc.typeOperation != :type')
                   ->andWhere('o.dateOperation <= :from')
                   //->andWhere('o.dateOperation >= :from')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['from' => $sDate, 'type' => $type, 'occasion' => $occasion])
                   ->select('SUM(o.amount) as totalcredit');
        
        return $qb->getQuery()->getResult();
    }
    
    
    
    public function countByDebitAndOccasionAll($occasion, $type)
    {
        
        $qb = $this->createQueryBuilder('o')
                   ->join('o.occasion', 'oc')
                    ->andWhere('oc = :occasion')
                   ->andWhere('oc.typeOperation = :type')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['type' => $type, 'occasion' => $occasion])
                   ->select('SUM(o.amount) as totaldebit');
        
        return $qb->getQuery()->getResult();
    }
    
   
    public function countByCreditAndOccasionAll($occasion, $type)
    {
       
        $qb = $this->createQueryBuilder('o')
                   ->join('o.occasion', 'oc')
                  ->andWhere('oc = :occasion')
                   ->andWhere('oc.typeOperation != :type')
                  
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['type' => $type, 'occasion' => $occasion])
                   ->select('SUM(o.amount) as totalcredit');
        
        return $qb->getQuery()->getResult();
    }
    
    
    
    
    public function findByDateAndUser($user, $sameDate)
    {
        $sDate = $sameDate;
        $eDate = $sameDate;
        if(is_array($sameDate)){
            $sDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]." 00:00:00");
            $eDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]." 23:59:59");
        }
        
        
        
        $qb = $this->createQueryBuilder('o')
                   ->andWhere('o.user = :user')
                   ->andWhere('o.dateOperation >= :from')
                   ->andWhere('o.dateOperation <= :to')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['from' => $sDate, 'to' => $eDate, 'user' => $user]);
        
        return $qb->getQuery()->getResult();
    }
    
    
    public function findByDate($sameDate)
    {
        $sDate = $sameDate;
        $eDate = $sameDate;
        if(is_array($sameDate)){
            $sDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]." 00:00:00");
            $eDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]." 23:59:59");
        }
        
        $qb = $this->createQueryBuilder('o')
                   ->andWhere('o.dateOperation >= :from')
                   ->andWhere('o.dateOperation <= :to')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['from' => $sDate, 'to' => $eDate]);
        
        return $qb->getQuery()->getResult();
    }
   
    public function findByDateAndHeadingAndUser($sameDate, $occasion, $user)
    {
        $sDate = $sameDate;
        $eDate = $sameDate;
        if(is_array($sameDate)){
            $sDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]." 00:00:00");
            $eDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]." 23:59:59");
        }
        
        $qb = $this->createQueryBuilder('o')
                   ->join('o.occasion', 'oc')
                   ->andWhere('oc.heading = :heading')
                   ->andWhere('o.dateOperation >= :from')
                ->andWhere('o.dateOperation <= :to')
                   //->andWhere('o.occasion = :occasion')
                   ->andWhere('o.user = :user')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['from' => $sDate, 'to' => $eDate, 'heading' => $occasion->getHeading(), 'user' => $user]);
        
        return $qb->getQuery()->getResult();
    }
    
     
    public function findByDateAndOccasion($sameDate, $occasion)
    {
        $sDate = $sameDate;
        $eDate = $sameDate;
        if(is_array($sameDate)){
            $sDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]." 00:00:00");
            $eDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]." 23:59:59");
        }
       
      
        $qb = $this->createQueryBuilder('o')
                   ->andWhere('o.dateOperation >= :from')
                   ->andWhere('o.occasion = :occasion')
                   ->andWhere('o.dateOperation <= :to')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['from' => $sDate, 'to' => $eDate, 'occasion' => $occasion]);
        //>select('SUM(o.amount) as total')
        return $qb->getQuery()->getResult();
    }
    
    public function findByDateAndOccasionSUM($sameDate, $occasion)
    {
        $sDate = $sameDate;
        $eDate = $sameDate;
        if(is_array($sameDate)){
            $sDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]." 00:00:00");
            $eDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]." 23:59:59");
        }
        
      
      
        $qb = $this->createQueryBuilder('o')
                   ->andWhere('o.dateOperation >= :from')
                   ->andWhere('o.dateOperation <= :to')
                   ->andWhere('o.occasion = :occasion')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['from' => $sDate, 'to' => $eDate, 'occasion' => $occasion])
                   ->select('SUM(o.amount) as total');
        //
        return $qb->getQuery()->getResult();
    }
    
    
    public function findByDateAndHeading($sameDate, $occasion)
    {
        $sDate = $sameDate;
        $eDate = $sameDate;
        if(is_array($sameDate)){
            $sDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]." 00:00:00");
            $eDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]." 23:59:59");
        }
        
        $qb = $this->createQueryBuilder('o')
                   ->join('o.occasion', 'oc')
                   ->andWhere('oc.heading = :heading')
                   ->andWhere('o.dateOperation >= :from')
                   ->andWhere('o.dateOperation <= :to')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['from' => $sDate, 'to' => $eDate, 'heading' => $occasion->getHeading()]);
        
        return $qb->getQuery()->getResult();
    }
    
    
    public function findByDateAndOccasionAndUser($sameDate, $occasion, $user)
    {
        $sDate = $sameDate;
        $eDate = $sameDate;
        if(is_array($sameDate)){
            $sDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]." 00:00:00");
            $eDate = new \DateTime($sameDate["year"]."-".$sameDate["month"]."-".$sameDate["day"]." 23:59:59");
        }
        
        
        $qb = $this->createQueryBuilder('o')
                   ->andWhere('o.dateOperation >= :from')
                   ->andWhere('o.dateOperation <= :to')
                   ->andWhere('o.occasion = :occasion')
                   ->andWhere('o.user = :user')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['from' => $sDate, 'to' => $eDate, 'occasion' => $occasion, 'user' => $user]);
        
        return $qb->getQuery()->getResult();
    }
    
    
    
    
    
    public function findBetweenDateAndOccasionAndUser($user, $occasion, $startDate, $endDate)
    {
        $sDate = $startDate;
        if(is_array($startDate)){
           $sDate = new \DateTime($startDate["year"]."-".$startDate["month"]."-".$startDate["day"]." 00:00:00");
        }
        
        $eDate = $endDate;
        if(is_array($endDate)){
           $eDate = new \DateTime($endDate["year"]."-".$endDate["month"]."-".$endDate["day"]." 23:59:59");
        }
        
        if(is_object($occasion)) {
            $qb = $this->createQueryBuilder('o')
                   ->andWhere('o.user = :user')
                   ->andWhere('o.occasion = :occasion')
                   ->andWhere('o.dateOperation >= :startDate')
                   ->andWhere('o.dateOperation <= :endDate')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['startDate' => $sDate, 'endDate' => $eDate, 'occasion' => $occasion, 'user' => $user]);
        
            return $qb->getQuery()->getResult();
        } else {
            $qb = $this->createQueryBuilder('o')
                   ->join('o.occasion', 'oc')
                   ->andWhere('o.user = :user')
                   ->andWhere('oc.name LIKE :occasion')
                   ->andWhere('o.dateOperation >= :startDate')
                   ->andWhere('o.dateOperation <= :endDate')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['startDate' => $sDate, 'endDate' => $eDate, 'occasion' => "%".$occasion."%", 'user' => $user]);
        
            return $qb->getQuery()->getResult();
        }
        
        
    }
   
    public function findBetweenDateAndOccasion($occasion, $startDate, $endDate)
    {
        $sDate = $startDate;
        if(is_array($startDate)){
           $sDate = new \DateTime($startDate["year"]."-".$startDate["month"]."-".$startDate["day"]." 00:00:00");
        }
        
        $eDate = $endDate;
        if(is_array($endDate)){
           $eDate = new \DateTime($endDate["year"]."-".$endDate["month"]."-".$endDate["day"]." 23:59:59");
        }
        
        if(is_object($occasion)) {
             $qb = $this->createQueryBuilder('o')
                   ->andWhere('o.occasion = :occasion')
                   ->andWhere('o.dateOperation >= :startDate')
                   ->andWhere('o.dateOperation <= :endDate')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['startDate' => $sDate, 'endDate' => $eDate, 'occasion' => $occasion]);
         
        } else {
              $qb = $this->createQueryBuilder('o')
                   ->join('o.occasion', 'oc')
                   ->andWhere('oc.name LIKE :occasion')
                   ->andWhere('o.dateOperation >= :startDate')
                   ->andWhere('o.dateOperation <= :endDate')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['startDate' => $sDate, 'endDate' => $eDate, 'occasion' => "%".$occasion."%"]);
        
        }
       
        return $qb->getQuery()->getResult();
    }
    
    public function findBetweenDateAndUser($user, $startDate, $endDate)
    {
        $sDate = $startDate;
        if(is_array($startDate)){
           $sDate = new \DateTime($startDate["year"]."-".$startDate["month"]."-".$startDate["day"]." 00:00:00");
        }
        
        $eDate = $endDate;
        if(is_array($endDate)){
           $eDate = new \DateTime($endDate["year"]."-".$endDate["month"]."-".$endDate["day"]." 23:59:59");
        }
      
        
        $qb = $this->createQueryBuilder('o')
                   ->andWhere('o.user = :user')
                   ->andWhere('o.dateOperation >= :startDate')
                   ->andWhere('o.dateOperation <= :endDate')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['startDate' => $sDate, 'endDate' => $eDate, 'user' => $user]);
        
        return $qb->getQuery()->getResult();
    }
    
    public function findBetweenDate($startDate, $endDate)
    {
        $sDate = $startDate;
        if(is_array($startDate)){
           $sDate = new \DateTime($startDate["year"]."-".$startDate["month"]."-".$startDate["day"]." 00:00:00");
        }
        
        $eDate = $endDate;
        if(is_array($endDate)){
           $eDate = new \DateTime($endDate["year"]."-".$endDate["month"]."-".$endDate["day"]." 23:59:59");
        }
        
        
        
        $qb = $this->createQueryBuilder('o')
                   ->andWhere('o.dateOperation >= :startDate')
                   ->andWhere('o.dateOperation <= :endDate')
                   ->orderBy('o.id', 'DESC')
                   ->setParameters(['startDate' => $sDate, 'endDate' => $eDate]);
        
        return $qb->getQuery()->getResult();
    }
    
    
    // /**
    //  * @return Operation[] Returns an array of Operation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Operation
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
