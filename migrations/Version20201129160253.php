<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201129160253 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE afri_card ADD agency_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE afri_card ADD CONSTRAINT FK_628AA830CDEADB2A FOREIGN KEY (agency_id) REFERENCES agency (id)');
        $this->addSql('CREATE INDEX IDX_628AA830CDEADB2A ON afri_card (agency_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE afri_card DROP FOREIGN KEY FK_628AA830CDEADB2A');
        $this->addSql('DROP INDEX IDX_628AA830CDEADB2A ON afri_card');
        $this->addSql('ALTER TABLE afri_card DROP agency_id');
    }
}
