<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201214234613 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE bank_flow (id INT AUTO_INCREMENT NOT NULL, amount VARCHAR(100) NOT NULL, type VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE coffer_state (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, orange VARCHAR(100) NOT NULL, moov VARCHAR(100) NOT NULL, mtn VARCHAR(100) NOT NULL, wizall VARCHAR(100) NOT NULL, wave VARCHAR(100) NOT NULL, cash VARCHAR(100) NOT NULL, currency VARCHAR(100) NOT NULL, other VARCHAR(100) NOT NULL, africard VARCHAR(100) NOT NULL, date_state DATE NOT NULL, INDEX IDX_6F206669A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE expense (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, amount VARCHAR(100) NOT NULL, objet VARCHAR(100) NOT NULL, INDEX IDX_2D3A8DA6A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE coffer_state ADD CONSTRAINT FK_6F206669A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE expense ADD CONSTRAINT FK_2D3A8DA6A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE bank_flow');
        $this->addSql('DROP TABLE coffer_state');
        $this->addSql('DROP TABLE expense');
    }
}
