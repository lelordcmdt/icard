<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210204233303 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE extra_cash (id INT AUTO_INCREMENT NOT NULL, safe INT NOT NULL, balance_uv INT NOT NULL, currency INT NOT NULL, debt INT NOT NULL, other INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE afri_card ADD date_save VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE agency DROP balance');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE extra_cash');
        $this->addSql('ALTER TABLE afri_card DROP date_save');
        $this->addSql('ALTER TABLE agency ADD balance VARCHAR(150) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
