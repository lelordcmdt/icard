<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201213103607 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE agency ADD balance VARCHAR(150) NOT NULL');
        $this->addSql('ALTER TABLE benefit ADD user_id INT DEFAULT NULL, ADD type VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE benefit ADD CONSTRAINT FK_5C8B001FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_5C8B001FA76ED395 ON benefit (user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE agency DROP balance');
        $this->addSql('ALTER TABLE benefit DROP FOREIGN KEY FK_5C8B001FA76ED395');
        $this->addSql('DROP INDEX IDX_5C8B001FA76ED395 ON benefit');
        $this->addSql('ALTER TABLE benefit DROP user_id, DROP type');
    }
}
